package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class BrightpeakNavMenuTest extends BrightpeakBaseTest {

  BrightpeakHome brightpeakHome = new BrightpeakHome();
//  private final Hyperlink getStartedLink = new Hyperlink(By.id("menu-item-get-started"));
//  private final Hyperlink productsLink = new Hyperlink(By.id("menu-item-products"));
//  private final Hyperlink resourcesLink = new Hyperlink(By.id("menu-item-resources"));
//  private final Hyperlink aboutLink = new Hyperlink(By.id("menu-item-about"));
//  private final Hyperlink blogLink = new Hyperlink(By.id("menu-item-tips-&-stories"));

  ArrayList<Hyperlink> linksList = new ArrayList<Hyperlink>(){{
        new Hyperlink(By.id("menu-item-get-started"));
        new Hyperlink(By.id("menu-item-products"));
        new Hyperlink(By.id("menu-item-resources"));
        new Hyperlink(By.id("menu-item-about"));
        new Hyperlink(By.id("menu-item-tips-&-stories"));
  }};


  /**
   * The Navigation Menu Checklist Test Method.
   *
   * @throws Throwable
   */
  @Test(description = "1 - NavMenuChecklist Questionnaire")
  public void navMenuChecklistTest() throws Throwable {
    for(Hyperlink h : linksList) {
      brightpeakHome.linkCheck(h);
    }
  }
}
