package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import org.testng.annotations.Test;

public class BrightpeakSmokeTest extends BrightpeakBaseTest {

    BrightpeakHome brightpeakHome = new BrightpeakHome();

    /**
     * The Term Life Insurance Smoke Test Method
     *
     * @throws Throwable
     */
    @Test(description = "1 - TLI Smoke Test", groups = "SmokeTests")
    public void termLifeInsuranceSmokeTest() throws Throwable {
        brightpeakHome.launchTLIPage()
                .clickGetMyFreeQuoteTLILink()
                .completeTLIAboutPageQuestions();
    }

    /**
     * The Disability Insurance Smoke Test Method
     *
     * @throws Throwable
     */
    @Test(description = "1 - DI Smoke Test", groups = "SmokeTests")
    public void disabilityInsuranceSmokeTest() throws Throwable {
        brightpeakHome.launchDIPage()
                .clickGetMyFreeQuoteLink()
                .completeDIQuickQuote()
                .completeDIAboutPageQuestions();
    }
}
