package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import org.testng.annotations.Test;

public class BrightpeakEssaTest extends BrightpeakBaseTest {

    BrightpeakHome brightpeakHome = new BrightpeakHome();

    /**
     * The ESSA Application Test Method
     *
     * @throws Throwable
     */
    @Test(description = "1 - ESSA Questionnaire")
    public void launchESSAPageTest() throws Throwable {
        brightpeakHome.launchESSAPage()
                .clickOpenLink()
                .clickBeginToGetStarted()
                .completeESSAAccountInfo()
                .completeESSPersonalInfoPage()
                .completeESSAFundAccountInfo()
                .completeESSARecurringDepositsPage()
                .completeESSAReviewPage()
                .completeESSAAgreeAndSubmit();
    }

}