package com.sqs.brightpeakTests;

import com.sqs.common.Keywords;
import com.sqs.pageobjects.BrightpeakHome;
import com.sqs.pageobjects.diPages.*;
import org.testng.annotations.Test;

public class BrightpeakDiErrors extends BrightpeakBaseTest {

  private BrightpeakHome brightpeakHome = new BrightpeakHome();
  private DiQuoteAboutPage diAboutPage = new DiQuoteAboutPage();
  private DiQuoteCoveragePage diCoveragePage = new DiQuoteCoveragePage();
  private DiQuoteEligibilityPage diEligibilityPage = new DiQuoteEligibilityPage();
  private DiQuoteEmploymentPage diEmploymentPage = new DiQuoteEmploymentPage();
  private DiQuoteExistingPage diExistingPage = new DiQuoteExistingPage();
  private DiQuoteDeclarationPage diDeclarationPage = new DiQuoteDeclarationPage();
  private DiQuotePaymentPage diPaymentPage = new DiQuotePaymentPage();
  private DiQuoteSubmissionPage diSubmissionPage = new DiQuoteSubmissionPage();

  /**
   * The Disability Insurance Quotation Negative Test Method - Error Messages
   *
   * @throws Throwable
   */

  //ABOUT PAGE ERRORS
  @Test(description = "DI Error - Step 9 - ZIP Error")
  public void diErrorZip() throws Throwable {
    brightpeakHome.launchDIPage()
        .clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .diZipFill("10001");
    assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.zipError).isDisplayed();
  }

  @Test(description = "DI Error - Step 11 - DOB too old Error")
  public void diErrorDobOld() throws Throwable {
    brightpeakHome.launchDIPage()
        .clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .diDOBFill("01011956");
    assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.dobError).isDisplayed();
  }

    @Test(description = "DI Error - Step 13 - DOB too young Error")
    public void diErrorDobYoung() throws Throwable {
      brightpeakHome.launchDIPage()
          .clickGetMyFreeQuoteLink()
          .completeDIQuickQuote()
          .diDOBFill("01012000");
      assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.dobError).isDisplayed();
    }

    @Test(description = "DI Error - Step 17 - Feet Error")
    public void diErrorFeet() throws Throwable {
      brightpeakHome.launchDIPage()
          .clickGetMyFreeQuoteLink()
          .completeDIQuickQuote()
          .diFeetFill("10");
      assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.feetError).isDisplayed();
    }

    @Test(description = "DI Error - Step 19 - Inches Error")
    public void diErrorInches() throws Throwable {
      brightpeakHome.launchDIPage()
          .clickGetMyFreeQuoteLink()
          .completeDIQuickQuote()
          .diInchesFill("12");
      assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.inchesError).isDisplayed();
    }

    @Test(description = "DI Error - Step 21 - Based on Height and weight")
    public void diHeightWeightError() throws Throwable {
      brightpeakHome.launchDIPage()
          .clickGetMyFreeQuoteLink()
          .completeDIQuickQuote()
          .diFeetFill("10")
          .diInchesFill("12")
          .diPoundsFill("400");
      assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.heightAndWeightError).isDisplayed();
    }

    @Test(description = "DI Error - Step 21 - Pounds")
    public void diErrorPounds() throws Throwable {
      brightpeakHome.launchDIPage()
          .clickGetMyFreeQuoteLink()
          .completeDIQuickQuote()
          .diFeetFill("10")
          .diInchesFill("12")
          .diPoundsFill("300");
      assert diAboutPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.poundsError).isDisplayed();
    }

  //COVERAGE PAGE ERRORS
  @Test(description = "DI Error - Step 26 - Annual Gross Income")
  public void diErrorCoverage() throws Throwable {
    brightpeakHome.launchDIPage()
        .clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .diCoverageFill("9000");
    assert diCoveragePage.getErrorMessageLabelElement(Keywords.Errors.DIPages.annualGrossError).isDisplayed();
  }

  //ELIGIBILITY PAGE ERRORS
    @Test(description = "DI Error - Step 35 - No Christian No Spouse")
  public void diErrorNoChristianNoSpouse() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diNoNoChristianSpouseFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 36 - Yes Christian No Spouse")
  public void diErrorYesChristianNoSpouse() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesNoChristianSpouseFill();
    assert(!diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed());
  }

  @Test(description = "DI Error - Step 38 - No Christian Yes Spouse")
  public void diErrorNoChristianYesSpouse() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diNoYesChristianSpouseFill();
    assert(!diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed());
  }

  @Test(description = "DI Error - Step  - Yes Christian Yes Spouse")
  public void diErrorYesChristianYesSpouse() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesYesChristianSpouseFill();
    assert(!diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed());
  }

  @Test(description = "DI Error - Step 39 - No US Citizen")
  public void diErrorNoUSCitizen() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diNoUSCitizenFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 41 - Yes Military")
  public void diErrorYesMilitary() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesMilitaryFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 43 - Yes Disability")
  public void diErrorYesDisability() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesDisabilityFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 45 - Yes AIDS")
  public void diErrorYesAids() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesAidsFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 47 - Yes Testing")
  public void diErrorYesTesting() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesTestingFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 49 - Yes Scheduled")
  public void diErrorYesScheduled() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesScheduledFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 51 - Yes Drug")
  public void diErrorYesDrug() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYesDrugFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 53 - Yes Treatment")
  public void diErrorYesTreatment() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diYestreatmentFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 54 - Yes Eligible")
  public void diErrorYesEligible() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEligibility()
        .diEligibleFill();
    assert diEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.youAreEligibleError).isDisplayed();
  }

  //EMPLOYMENT PAGE ERRORS
  @Test(description = "DI Error - Step 63 - Self Employed")
  public void diErrorSelfEmployed() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEmployment()
        .diYesSelfEmployedFill();
    assert diEmploymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.selfEmployedError).isDisplayed();
  }

  @Test(description = "DI Error - Step 65 - Second Income")
  public void diErrorSecondIncome() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEmployment()
        .diYesSecondIncomeFill();
    assert diEmploymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.secondIncomeError).isDisplayed();
  }

  @Test(description = "DI Error - Step 75 - Max months worked")
  public void diErrorMaxMonths() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToEmployment()
        .diMonthsWorkedFill("13");
    assert diEmploymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.maxMonthsError).isDisplayed();
  }

  //EXISTING PAGE ERRORS
    @Test(description = "DI Error - Step 79 - Other Disability Income")
  public void diErrorOtherDisabilityIncome() throws Throwable {
    brightpeakHome.launchDIPage()
        .clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo()
        .completeDIEmploymentQuestion()
        .diYesExistingOtherDisabilityIncomeFill();
    assert diExistingPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.otherDisabilityError).isDisplayed();
  }

  //PRELIMINARY DECLARATION PAGE ERRORS
    @Test(description = "DI Error - Step 83 - Yes Diagnosed")
  public void diErrorYesDiagnosed() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToPrelimDeclaration()
        .diYesDiagnosedFill();
    assert diDeclarationPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  @Test(description = "DI Error - Step 85 - Yes Been Advised test/surgery")
  public void diErrorYesAdvised() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToPrelimDeclaration()
        .diYesAdvisedFill();
    assert diDeclarationPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.genericError).isDisplayed();
  }

  //PAYMENT PAGE ERRORS
  @Test(description = "DI Error - Step 88 - No Application Payment")
  public void diErrorNoApplicationPayment() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToPayment()
        .diNoApplicationPaymentFill();
    assert diPaymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.applicationPaymentError).isDisplayed();
  }

  @Test(description = "DI Error - Step 90 - No Responsible Payment")
  public void diErrorNoResponsiblePayment() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToPayment()
        .diNoResponsiblePaymentFill();
    assert diPaymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.responsiblePaymentError).isDisplayed();
  }

  @Test(description = "DI Error - Step 94 - Routing Number")
  public void diErrorRoutingNumber() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToPayment()
        .diRoutingNoFill("funny");
    assert diPaymentPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.responsiblePaymentError).isDisplayed();
  }

  //SUBMISSION PAGE ERRORS
   @Test(description = "DI Error - Step 101 - Signed State")
  public void diErrorSignedState() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToSubmission()
        .diSignedStateErrorFill("Alabama");
    assert diSubmissionPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.signedStateError).isDisplayed();
  }

  @Test(description = "DI Error - Step 103 - Electronic delivery")
  public void diErrorElectronicDelivery() throws Throwable {
    brightpeakHome.launchDIPage()
        .completeUpToSubmission()
        .diElectronicDeliveryFill();
    assert diSubmissionPage.getErrorMessageLabelElement(Keywords.Errors.DIPages.electronicDeliveryError).isDisplayed();
  }

}