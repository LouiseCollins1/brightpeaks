package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import org.testng.annotations.Test;

public class BrightpeakTliTest extends BrightpeakBaseTest {

  BrightpeakHome brightpeakHome = new BrightpeakHome();

  /**
   * The Term Life Insurance Quotation Test Method
   *
   * @throws Throwable error
   */
  @Test(description = "1 - TLI Questionnaire")
  public void launchTermLifeInsurancePage() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestions()
        .completeProposedInsuredDetails()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails()
        .completePaymentConfirmation();
  }

  /**
   * The Term Life Insurance Quotation Test Method for Alternative Path One
   *
   * @throws Throwable error
   */
  @Test(description = "1 - TLI AltPath1")
  public void termLifeInsuranceAltPathOne() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestionsAltPath1()
        .clickOnMinusAddAndAdjustCover()
        .completeTLIQuoteEligibilityQuestions()
        .completeProposedInsuredDetailsAltPathOne()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails()
        .completePaymentConfirmation();
  }

  /**
   * The Term Life Insurance Quotation Test Method for Alternative Path Two
   *
   * @throws Throwable error
   */
  @Test(description = "1 - TLI AltPath2")
  public void termLifeInsuranceAltPathTwo() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestionsAltPathTwo()
        .completeProposedInsuredDetails()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails()
        .completePaymentConfirmation();
  }

  /**
   * The Term Life Insurance Quotation Test Method for Alternative Path Three
   *
   * @throws Throwable error
   */
  @Test(description = "1 - TLI AltPath3")
  public void termLifeInsuranceAltPathThree() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestionsAltPathThree()
        .completeProposedInsuredDetails()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails()
        .completePaymentConfirmation();
  }

  /**
   * The Term Life Insurance Quotation Test Method for Alternative Path Four
   *
   * @throws Throwable error
   */
  @Test(description = "1 - TLI AltPath4")
  public void termLifeInsuranceAltPathFour() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestions()
        .completeTLIQuoteEligibilityQuestionsAltPathFour()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails()
        .completePaymentConfirmation();
  }
}
