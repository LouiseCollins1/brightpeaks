package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import org.testng.annotations.Test;

public class BrightpeakDiTest extends BrightpeakBaseTest {

  BrightpeakHome brightpeakHome = new BrightpeakHome();

  /**
   * The Disability Insurance Quotation Test Method
   *
   * @throws Throwable
   */
  @Test(description = "1 - DI Questionnaire", groups="SmokeTests")
  public void launchDisabilityInsurancePageTest() throws Throwable {
    brightpeakHome.launchDIPage()
        .clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo()
        .completeDIEmploymentQuestion()
        .completeDIExistingQuestion()
        .completeDIDeclarationQuestion()
        .completeDIPaymentQuestion()
        .completeDISubmissionQuestions();
  }

//  /**
//   * The Disability Insurance Quotation Test Method for Alternative Path One.
//   *
//   * @throws Throwable error
//   */
//  @Test(description = "1 - DI AltPath1")
//  public void disabilityInsuranceAltPathOne() throws Throwable {
//    brightpeakHome.launchDIPage()
//        .clickGetMyFreeQuoteLink()
//        .completeDIQuickQuote()
//        .completeDIAboutPageQuestions()
//        .completeDIOccupationQuestion()
//        .completeDICoverageQuestion()
//        .completeDIResultsQuestion()
//        .completeDIQuoteEligibilityQuestionsAltPathOne()
//        .completeDIPersonalInfo()
//        .completeDIEmploymentQuestion()
//        .completeDIExistingQuestion()
//        .completeDIDeclarationQuestion()
//        .completeDIPaymentQuestion()
//        .completeDISubmissionQuestions();
//  }
//
//  /**
//   * The Disability Insurance Quotation Test Method for Alternative Path two.
//   *
//   * @throws Throwable error
//   */
//  @Test(description = "1 - DI AltPath2")
//  public void disabilityInsuranceAltPathTwo() throws Throwable {
//    brightpeakHome.launchDIPage()
//        .clickGetMyFreeQuoteLink()
//        .completeDIQuickQuote()
//        .completeDIAboutPageQuestions()
//        .completeDIOccupationQuestion()
//        .completeDICoverageQuestion()
//        .completeDIResultsQuestion()
//        .completeDIQuoteEligibilityQuestionsAltPathTwo()
//        .completeDIPersonalInfo()
//        .completeDIEmploymentQuestion()
//        .completeDIExistingQuestion()
//        .completeDIDeclarationQuestion()
//        .completeDIPaymentQuestion()
//        .completeDISubmissionQuestions();
//  }
//
//  /**
//   * The Disability Insurance Quotation Test Method for Alternative Path Three.
//   *
//   * @throws Throwable error
//   */
//  @Test(description = "1 - DI AltPath3")
//  public void disabilityInsuranceAltPathThree() throws Throwable {
//    brightpeakHome.launchDIPage()
//        .clickGetMyFreeQuoteLink()
//        .completeDIQuickQuote()
//        .completeDIAboutPageQuestions()
//        .completeDIOccupationQuestion()
//        .completeDICoverageQuestion()
//        .completeDIResultsQuestion()
//        .completeDIQuoteEligibilityQuestions()
//        .completeDIPersonalInfo()
//        .completeDIQuoteEligibilityQuestionsAltPathThree()
//        .completeDIExistingQuestion()
//        .completeDIDeclarationQuestion()
//        .completeDIPaymentQuestion()
//        .completeDISubmissionQuestions();
//  }

}
