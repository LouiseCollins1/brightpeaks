package com.sqs.brightpeakTests;

import com.sqs.pageobjects.BrightpeakHome;
import org.testng.annotations.Test;

public class BrightpeakGripTest extends BrightpeakBaseTest {

    BrightpeakHome brightpeakHome = new BrightpeakHome();

    /**
     * The GRIP Questionnaire Test Method
     *
     * @throws Throwable
     */
    @Test(description = "1 - GRIP Questionnaire")
    public void launchGRIPPageTest() throws Throwable {
        brightpeakHome.launchGRIPPage()
                .clickGetMyFreeQuoteLink()
                .completeGRIPQuestionsAndSubmit()
                .verifyInformationMessageDisplayed();
    }

}