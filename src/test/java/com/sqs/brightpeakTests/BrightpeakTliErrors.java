package com.sqs.brightpeakTests;

import com.sqs.common.Keywords;
import com.sqs.common.PageObject;
import com.sqs.pageobjects.BrightpeakHome;
import com.sqs.pageobjects.tliPages.*;
import org.testng.annotations.Test;

public class BrightpeakTliErrors extends BrightpeakBaseTest {

  private BrightpeakHome brightpeakHome = new BrightpeakHome();
  private TliQuoteAboutPage tliAboutPage = new TliQuoteAboutPage();
  private TliQuoteNeedsPage tliNeedsPage = new TliQuoteNeedsPage();
  private TliQuoteEligibilityPage tliEligibilityPage = new TliQuoteEligibilityPage();
  private TliQuoteProposedInsuredPage tliProposedInsuredPage = new TliQuoteProposedInsuredPage();
  private TliQuotePrelimDeclarationPage tliPrelimDeclarationPage = new TliQuotePrelimDeclarationPage();
  private TliQuotePaymentPage tliPaymentPage = new TliQuotePaymentPage();

  /**
   * The Term Life Insurance Quotation Negative Test Method - Error Messages
   *
   * @throws Throwable
   */

  //ABOUT PAGE ERRORS
  @Test(description = "TLI Error - Step 10 - DOB too old Error")
  public void tliErrorDobOld() throws Throwable {
      brightpeakHome.launchTLIPage()
          .clickGetMyFreeQuoteTLILink()
        .tliDOBFill("01011941");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.dobError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 12 - DOB too young Error")
  public void tliErrorDobYoung() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .tliDOBFill("01012000");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.dobError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 16 - Feet Error")
  public void tliErrorFeet() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .tliFeetFill("10");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.feetError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 18 - Inches Error")
  public void tliErrorInches() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .tliInchesFill("12");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.inchesError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 20 - Based on Height and weight")
  public void tliHeightWeightError() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .tliFeetFill("10")
        .tliInchesFill("12")
        .tliPoundsFill("400");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.heightAndWeightError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 20 - Pounds")
  public void tliErrorPounds() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .tliFeetFill("10")
        .tliInchesFill("12")
        .tliPoundsFill("300");
    assert tliAboutPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.poundsError).isDisplayed();
  }

  //NEED PAGE ERRORS
  @Test(description = "TLI Error - Step 29 - Desired Coverage Amount")
  public void tliErrorDesiredCoverage() throws Throwable {
    brightpeakHome.launchTLIPage()
        .clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .tliCoverageFill("9000");
    assert tliNeedsPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.desiredCoverageError).isDisplayed();
  }

  //ELIGIBILITY PAGE ERRORS
  @Test(description = "TLI Error - Step 36 - No Christian No Spouse")
  public void tliErrorNoChristianNoSpouse() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliNoNoChristianSpouseFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 37 - Yes Christian No Spouse")
  public void tliErrorYesChristianNoSpouse() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesNoChristianSpouseFill();
    assert(!tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed());
  }

  @Test(description = "TLI Error - Step 39 - No Christian Yes Spouse")
  public void tliErrorNoChristianYesSpouse() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliNoYesChristianSpouseFill();
    assert(!tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed());
  }

  @Test(description = "TLI Error - Step  - Yes Christian Yes Spouse")
  public void tliErrorYesChristianYesSpouse() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesYesChristianSpouseFill();
    assert(!tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed());
  }


  @Test(description = "TLI Error - Step 40 - No US Citizen")
  public void tliErrorNoUSCitizen() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliNoUSCitizenFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 42 - Yes Other TLI")
  public void tliErrorYesOtherTLI() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesOtherTliFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 44 - Yes AIDS")
  public void tliErrorYesAids() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesAidsFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 46 - Yes Medical Conditions")
  public void tliErrorYesMedicalConditions() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesMedicalconditionsFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 48 - Yes Procedures")
  public void tliErrorYesProcedures() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesProceduresFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 50 - Yes Drink Drug Abuse")
  public void tliErrorYesDrinkDrugAbuse() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesDrinkDrugAbuseFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 52 - Yes Listed Conditions")
  public void tliErrorYesListedConditions() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliYesListedConditionsFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 53 - You are Eligible!")
  public void tliVerifyEligibility() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsPages()
        .tliEligibleFill();
    assert tliEligibilityPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.youAreEligible).isDisplayed();
  }

  //PROPOSED INSURED PAGE ERRORS
  @Test(description = "TLI Error - Step 65 - Yes Military")
  public void tliErrorYesMilitary() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsEligibilityPages()
        .tliYesMilitaryFill();
    assert tliProposedInsuredPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.militaryMemberError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 67 - Yes ThirdParty")
  public void tliErrorYesThirdParty() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsEligibilityPages()
        .tliYesThirdPartyFill();
    assert tliProposedInsuredPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  //PRELIMINARY DECLARATION PAGE ERRORS
  @Test(description = "TLI Error - Step 76 - Yes Past Six Years")
  public void tliErrorYesPastSixYears() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsEligibilityPrelimPages()
        .tliYesPastSixYearsFill();
    assert tliPrelimDeclarationPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  @Test(description = "TLI Error - Step 78 - Yes Past Six Days")
  public void tliErrorYesPastSixDays() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsEligibilityPrelimPages()
        .tliYesPastSixDaysFill();
    assert tliPrelimDeclarationPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError).isDisplayed();
  }

  //PAYMENT PAGE ERRORS
  @Test(description = "TLI Error - Step 85 - No Payment Responsible")
  public void tliErrorNoPaymentResponsible() throws Throwable {
    brightpeakHome.launchTLIPage()
        .completeTliAboutQuoteNeedsResultsEligibilityPrelimPaymentPages()
        .tliNoPaymentResponsibleFill();
    assert tliPaymentPage.getErrorMessageLabelElement(Keywords.Errors.TLIPages.premiumPaymentError).isDisplayed();
  }

}
