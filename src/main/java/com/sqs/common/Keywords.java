package com.sqs.common;

public class Keywords {
  public static class Errors {
    public static class TLIPages {
      public static String dobError = "Date of Birth";
      public static String feetError = "Feet";
      public static String inchesError = "Inches";
      public static String heightAndWeightError = "Based upon your height and weight";
      public static String poundsError = "No big deal, but based ";
      public static String desiredCoverageError = "Coverage amount out of range";
      public static String genericError = "Based on your response above,";
      public static String youAreEligible = "You are eligible!";
      public static String militaryMemberError = "You may be eligible for coverage";
      public static String premiumPaymentError = "We currently can't assist you online";

    }

    public static class DIPages {
      public static String zipError = "What a bummer";
      public static String dobError = "Date of Birth";
      public static String feetError = "Feet";
      public static String inchesError = "Inches";
      public static String heightAndWeightError = "Based upon your height and weight";
      public static String poundsError = "No big deal, but based ";
      public static String annualGrossError = "*$10,000 minimum";
      public static String genericError = "Based on your response above,";
      public static String youAreEligibleError = "You are eligible!";
      public static String selfEmployedError = "Some self-employed occupations are tricky to insure";
      public static String secondIncomeError = "Try as we might to keep them simple";
      public static String maxMonthsError = "Max months is 11";
      public static String otherDisabilityError = "These types of applications can get pretty complicated.";
      public static String applicationPaymentError = "We need to collect some additional information";
      public static String responsiblePaymentError = "We are unable to assist you online";
      public static String routingError = "Sorry, that routing number is invalid.";
      public static String signedStateError = "we are only able to process your online application if you will be signing in the state";
      public static String electronicDeliveryError = "we only support electronic delivery for our online application process";
    }
  }
}
