package com.sqs.common;

import com.sqs.core.common.CommonActions;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.common.ClickableElement;

import org.openqa.selenium.By;

public class Dropdown extends ClickableElement {
  private String dropdownName;

  public Dropdown(String dropdownName) {
    this.dropdownName = dropdownName;
  }

  public void selectByText(String dropdownOption) {
    this.selectByText(dropdownOption, this.timeout);
  }

  public void selectByText(String dropdownOption, int timeout) {
    CommonActions.pauseTest(1);

    try {
      (new ClickableElement(By.xpath("//md-select[contains(@aria-label,'" + this.dropdownName + "')] | //md-select[contains(@name,'" + this.dropdownName + "')] | //span[contains(text(),'" + this.dropdownName + "')]"))).click();
      CommonActions.pauseTest(1);
      (new ClickableElement(By.xpath("//*[@ng-value='period.coveragePeriod']//div[contains(text(),'" + dropdownOption + "')] | //*[@class='md-select-menu-container md-active md-clickable']//div[contains(text(), '" + dropdownOption + "')] | //option[contains(text(), '" + dropdownOption + "')] | //md-option[contains(normalize-space(),'" + dropdownOption + "')]/div | //md-option[contains(@value,'" + dropdownOption + "')]/div | //md-option[contains(@ng-reflect-value,'" + dropdownOption + "')]/div"))).click();
    } catch (Exception var4) {
      WebLog.error("Could not select " + dropdownOption + " for Dropdown " + this.dropdownName);
    }

  }
}
