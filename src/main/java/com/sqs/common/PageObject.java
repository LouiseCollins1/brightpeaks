package com.sqs.common;

import static org.hamcrest.MatcherAssert.assertThat;

import com.sqs.core.common.CommonActions;
import com.sqs.core.common.Config;
import com.sqs.core.sqslibs.Log;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.common.ClickableElement;
import com.sqs.web.elements.common.Element;
import org.hamcrest.Matcher;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Base page object.
 */
public abstract class PageObject {

  /**
   * method that instantiates a new page object
   * and verifies the page has loaded.
   *
   * @param aclass the page object class to instantiate
   * @param <D>    the page object class to instantiate
   * @return the page object class
   */
  public <D extends PageObject> D navigatingTo(Class<D> aclass) {

    D newPage = null;
    try {
      WebLog.info("Navigating to page: '" + aclass.getSimpleName() + "'");
      newPage = aclass.newInstance();
      waitUntilPageSpinnerUpdates();
      //Ensure we are on the correct page
      newPage.onPage();
    } catch (Exception ex) {
      WebLog.info(
          "Unable to navigate to page: '" + aclass.getSimpleName() + "' due to exception: " + ex
              .toString());
    }
    return newPage;
  }

  @Step("Asserting that element: {0}")
  public <T> void assertThatElement(String info, T actual, Matcher<? super T> matcher) {
    assertThat(info, actual, matcher);
  }

  public void OnPageElementMethod(Element elem, int timeout) {
    int i = 0;
    do {
      //if the on page element is on screen then exit
      if (elem.isPresent(1)) {
        return;
      }
      CommonActions.pauseTest(1);
      i++;
    } while (i < timeout);

    Log.testError("ERROR - not on the correct page.", new Exception("The page is missing input element verified on the OnPage method"));
  }

  public void OnPageElementMethod(Element elem){
    OnPageElementMethod(elem, Integer.parseInt(Config.getGlobalProperty("Timeout")));
  }

  public abstract void onPage();

  private void waitUntilPageSpinnerUpdates() throws InterruptedException {
    ClickableElement loadingSpinnerImage = new ClickableElement(By.xpath("//div[@class='cg-busy-default-spinner']/div"));

    int timeout = Integer.parseInt(Config.getGlobalProperty("Timeout"));
    int i = 0;
    do {
      //if Spinner is not displayed then exit
      if (!loadingSpinnerImage.isDisplayed(1)) {
        return;
      }
//      Thread.sleep(1000);
      CommonActions.pauseTest(1);
      i++;
    } while (i < timeout);
  }

  /**
   * Gets info element.
   *  
   * @param errorKeywords the information message
   * @return the info element
   */

  public Label getErrorMessageLabelElement(String errorKeywords) {
    return new Label(By.xpath("//label[contains(text(),'" + errorKeywords + "')]/../div |  //span[contains(text(), '" + errorKeywords + "')] | //div[contains(text(), '" + errorKeywords + "')]"));
  }

}
