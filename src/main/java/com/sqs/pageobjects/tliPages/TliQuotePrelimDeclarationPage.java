package com.sqs.pageobjects.tliPages;

import com.sqs.common.Keywords;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuotePrelimDeclarationPage.
 */

public class TliQuotePrelimDeclarationPage extends PageObject {

  //Links
  private static final String URL = "https://test.brightpeakfinancial.com/product/term-quote/#/quote/prelim-declaration";
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton noPastSixMonthRadio = new RadioButton(By.xpath("//*[@name='diagnosed']/*[@aria-label='NO']"));
  private final RadioButton yesPastSixMonthRadio = new RadioButton(By.xpath("//*[@name='diagnosed']/*[@aria-label='YES']"));
  private final RadioButton noPastSixtyDaysRadio = new RadioButton(By.xpath("//*[@name='futureProcedure']/*[@aria-label='NO']"));
  private final RadioButton yesPastSixtyDaysRadio = new RadioButton(By.xpath("//*[@name='futureProcedure']/*[@aria-label='YES']"));
  //Button
  private final Button next = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public TliQuotePrelimDeclarationPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Preliminary Declaration Page has successfully loaded.");
  }

  /**
   * Complete the TliQuotePrelimDeclarationPage details
   *
   * @return the TliQuoteBeneficiaryPage object
   */
  @Step("Complete TLI Preliminary Declaration Page Questions")
  public TliQuoteBeneficiaryPage completeDeclarationDetails() {
    noPastSixMonthRadio.setRadioButton();
    noPastSixtyDaysRadio.setRadioButton();
    next.click();
    return navigatingTo(TliQuoteBeneficiaryPage.class);
  }

  @Step("Complete TLI Preliminary Declaration Page Error Messages")
  public TliQuoteBeneficiaryPage tliPrelimDeclarationPageErrorMessages() {
    yesPastSixMonthRadio.setRadioButton();
    assert (getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError)).isPresent();
    noPastSixMonthRadio.setRadioButton();
    yesPastSixtyDaysRadio.setRadioButton();
    assert (getErrorMessageLabelElement(Keywords.Errors.TLIPages.genericError)).isPresent();
    noPastSixtyDaysRadio.setRadioButton();
    next.click();
    return navigatingTo(TliQuoteBeneficiaryPage.class);
  }

  /**Enter the negative user details for the TLI Preliminary Declaration page Error Messages
   *
   *
   */

  @Step("Fill in the TLI Preliminary Declaration Page Yes Past Six Months")
  public TliQuotePrelimDeclarationPage tliYesPastSixYearsFill() {
    yesPastSixMonthRadio.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Preliminary Declaration Page Yes Past Six Days")
  public TliQuotePrelimDeclarationPage tliYesPastSixDaysFill() {
    yesPastSixtyDaysRadio.setRadioButton();
    return this;
  }

}