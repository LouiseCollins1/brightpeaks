package com.sqs.pageobjects.tliPages;

import com.sqs.common.Keywords;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteEligibilityPage.
 */
public class TliQuoteEligibilityPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));

  //RadioButtons
  private final RadioButton yesChristianRdo = new RadioButton(By.xpath("//*[@id='christian']/*[@aria-label='YES']"));
  private final RadioButton yesChristianSpouseRdo = new RadioButton(By.xpath("//*[@id='spouse']/*[@aria-label='YES']"));
  private final RadioButton yesUSCitizenRdo = new RadioButton(By.xpath("//*[@id='citizen']/*[@aria-label='YES']"));
  private final RadioButton noChristianRdo = new RadioButton(By.xpath("//*[@id='christian']/*[@aria-label='NO']"));
  private final RadioButton noChristianSpouseRdo = new RadioButton(By.xpath("//*[@id='spouse']/*[@aria-label='NO']"));
  private final RadioButton noUSCitizenRdo = new RadioButton(By.xpath("//*[@id='citizen']/*[@aria-label='NO']"));
  private final RadioButton noOtherTLIRdo = new RadioButton(By.xpath("//*[@id='disability']/*[@aria-label='NO']"));
  private final RadioButton noAIDSRdo = new RadioButton(By.xpath("//*[@id='aids']/*[@aria-label='NO']"));
  private final RadioButton noMedicalConditionsRdo = new RadioButton(By.xpath("//*[@id='testing']/*[@aria-label='NO']"));
  private final RadioButton noProceduresRdo = new RadioButton(By.xpath("//*[@id='scheduled']/*[@aria-label='NO']"));
  private final RadioButton noDrinkDrugAbuseRdo = new RadioButton(By.xpath("//*[@id='drug']/*[@aria-label='NO']"));
  private final RadioButton noListedConditionsRdo = new RadioButton(By.xpath("//*[@id='treatment']/*[@aria-label='NO']"));
  private final RadioButton yesOtherTLIRdo = new RadioButton(By.xpath("//*[@id='disability']/*[@aria-label='YES']"));
  private final RadioButton yesAIDSRdo = new RadioButton(By.xpath("//*[@id='aids']/*[@aria-label='YES']"));
  private final RadioButton yesMedicalConditionsRdo = new RadioButton(By.xpath("//*[@id='testing']/*[@aria-label='YES']"));
  private final RadioButton yesProceduresRdo = new RadioButton(By.xpath("//*[@id='scheduled']/*[@aria-label='YES']"));
  private final RadioButton yesDrinkDrugAbuseRdo = new RadioButton(By.xpath("//*[@id='drug']/*[@aria-label='YES']"));
  private final RadioButton yesListedConditionsRdo = new RadioButton(By.xpath("//*[@id='treatment']/*[@aria-label='YES']"));

  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  /**
   * Instantiates a new Brightpeak TliQuoteEligibilityPage.
   */
  public TliQuoteEligibilityPage() {

  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Eligibility Page has successfully loaded.");
  }

  /**
   * Complete the TLIQuoteEligibility details for the user
   *
   * @return the TLIQuoteEligibility page object
   */

  @Step("Complete TLI Eligibility Page Questions")
  public TliQuoteProposedInsuredPage completeTLIQuoteEligibilityQuestions() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noOtherTLIRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noMedicalConditionsRdo.setRadioButton();
    noProceduresRdo.setRadioButton();
    noDrinkDrugAbuseRdo.setRadioButton();
    noListedConditionsRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(TliQuoteProposedInsuredPage.class);
  }

  /**
   * Complete the TLIQuoteEligibility details for alternative path two.
   *
   * @return the TLIQuoteEligibility page object
   */

  @Step("Complete TLI Eligibility Page Questions for AltPath2")
  public TliQuoteProposedInsuredPage completeTLIQuoteEligibilityQuestionsAltPathTwo() {
    noChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noOtherTLIRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noMedicalConditionsRdo.setRadioButton();
    noProceduresRdo.setRadioButton();
    noDrinkDrugAbuseRdo.setRadioButton();
    noListedConditionsRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(TliQuoteProposedInsuredPage.class);
  }

  /**
   * Complete the TLIQuoteEligibility details for alternative path two.
   *
   * @return the TLIQuoteEligibility page object
   */

  @Step("Complete TLI Eligibility Page Questions for AltPath3")
  public TliQuoteProposedInsuredPage completeTLIQuoteEligibilityQuestionsAltPathThree() {
    yesChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noOtherTLIRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noMedicalConditionsRdo.setRadioButton();
    noProceduresRdo.setRadioButton();
    noDrinkDrugAbuseRdo.setRadioButton();
    noListedConditionsRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(TliQuoteProposedInsuredPage.class);
  }

  /**
   * Enter the negative user details for the TLI Eligibility page Error Messages.
   */

  @Step("Fill in the TLI Eligibility Page No Christian No Spouse")
  public TliQuoteEligibilityPage tliNoNoChristianSpouseFill() {
    noChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Christian No Spouse")
  public TliQuoteEligibilityPage tliYesNoChristianSpouseFill() {
    yesChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page No Christian No Spouse")
  public TliQuoteEligibilityPage tliNoYesChristianSpouseFill() {
    noChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Christian Yes Spouse")
  public TliQuoteEligibilityPage tliYesYesChristianSpouseFill() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page No US Citizen")
  public TliQuoteEligibilityPage tliNoUSCitizenFill() {
    noUSCitizenRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Other TLI")
  public TliQuoteEligibilityPage tliYesOtherTliFill() {
    yesOtherTLIRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes AIDS")
  public TliQuoteEligibilityPage tliYesAidsFill() {
    yesAIDSRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Medical Conditions")
  public TliQuoteEligibilityPage tliYesMedicalconditionsFill() {
    yesMedicalConditionsRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Procedures")
  public TliQuoteEligibilityPage tliYesProceduresFill() {
    yesProceduresRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Drink Drug Abuse")
  public TliQuoteEligibilityPage tliYesDrinkDrugAbuseFill() {
    yesDrinkDrugAbuseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Yes Listed Conditions")
  public TliQuoteEligibilityPage tliYesListedConditionsFill() {
    yesListedConditionsRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Eligibility Page Eligibility")
  public TliQuoteEligibilityPage tliEligibleFill() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noOtherTLIRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noMedicalConditionsRdo.setRadioButton();
    noProceduresRdo.setRadioButton();
    noDrinkDrugAbuseRdo.setRadioButton();
    noListedConditionsRdo.setRadioButton();
    return this;
  }

}