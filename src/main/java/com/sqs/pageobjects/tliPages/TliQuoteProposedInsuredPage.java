package com.sqs.pageobjects.tliPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteProposedInsuredPage.
 */
public class TliQuoteProposedInsuredPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput streetAddressInput = new TextInput(By.xpath(".//*[@name='address1']"));
  private final TextInput streetAddressLineTwoInput = new TextInput(By.xpath(".//*[@name='address2']"));
  private final TextInput cityInput = new TextInput(By.xpath(".//input[@ng-model='insured.city']"));
  private final TextInput socialSecurityInput = new TextInput(By.xpath(".//*[@name='ssn']"));
  private final TextInput occupationInput = new TextInput(By.xpath(".//*[@name='currentoccupation']"));
  private final TextInput annualIncomeInput = new TextInput(By.xpath(".//*[@name='annualearnedincome']"));
  private final TextInput driversLicenseNumInput = new TextInput(By.xpath(".//*[@name='driversLicense']"));
  private final TextInput otherOccupationInput = new TextInput(By.xpath(".//input[@name='otheroccupation']"));
  private final TextInput otherEarnedIncomeInput = new TextInput(By.xpath(".//input[@name='otherearnedincome']"));

  //Dropdown
  private final Dropdown stateBornDropD = new Dropdown("State where you were born");
  private final Dropdown stateSignedDropD = new Dropdown("state");
  private final Dropdown stateBornInput = new Dropdown("State where you were born");
  private final Dropdown stateSigned = new Dropdown("state");

  //workaround for state dropdown
  private final Select stateSelect = new Select(By.xpath("//*[@ng-model='stateSigningSelection']"));
  private final Select stateValue = new Select(By.xpath("//md-option[@value='MN']"));

  //RadioButtons
  private final RadioButton afterFiveContactTimeRdo = new RadioButton(By.xpath(".//*[@name='timeToCall']/*[@value='NIGHT']"));
  private final RadioButton afterNoonRdo = new RadioButton(By.xpath("//*[@value='AFTERNOON']"));
  private final RadioButton noSecondIncomeRdo = new RadioButton(By.xpath(".//*[@name='secondaryincome']/*[@value='false']"));
  private final RadioButton yesSecondIncomeRdo = new RadioButton(By.xpath(".//*[@name='secondaryincome']/*[@value='true']"));
  private final RadioButton noMilitaryRdo = new RadioButton(By.xpath(".//*[@name='militaryquestion']/*[@value='false']"));
  private final RadioButton yesMilitaryRdo = new RadioButton(By.xpath(".//*[@name='militaryquestion']/*[@value='true']"));
  private final RadioButton noThirdPartyRdo = new RadioButton(By.xpath(".//*[@name='thirdpartycontract']/*[@value='false']"));
  private final RadioButton yesThirdPartyRdo = new RadioButton(By.xpath(".//*[@name='thirdpartycontract']/*[@value='true']"));
  private final RadioButton yesElectronicDeliveryRdo = new RadioButton(By.xpath(".//*[@name='electronicsub']/*[@value='true']"));
  //Button
  private final Button nextbtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));
  //Member Variables
  String streetAddress = "123 Oak St.";
  String streetAddressLineTwo = "#100";
  String city = "Minneapolis";
  String socialSecurity = "476" + RandomStringUtils.randomNumeric(6);
  String driversLicense = "C" + RandomStringUtils.randomNumeric(11);
  String occupation = "IT CONSULTANT";
  String anIncome = "100000";
  String otherOcc = "Basket Weaver";
  String otherIncome = "10000";

  public TliQuoteProposedInsuredPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Proposed Insured Page has successfully loaded.");
  }

  /**
   * Complete the Proposed Insured details
   *
   * @return the TliQuoteExistingPage  object
   */

  @Step("Complete TLI Proposed Insured Page Questions")
  public TliQuoteExistingPage completeProposedInsuredDetails() {
    streetAddressInput.setText(streetAddress);
    streetAddressLineTwoInput.setText(streetAddressLineTwo);
    cityInput.setText(city);
    stateBornDropD.selectByText("Minnesota");
    socialSecurityInput.setText(socialSecurity);
    driversLicenseNumInput.setText(driversLicense);
    afterFiveContactTimeRdo.setRadioButton();
    occupationInput.setText(occupation);
    annualIncomeInput.setText(anIncome);
    noSecondIncomeRdo.setRadioButton();
    noMilitaryRdo.setRadioButton();
    noThirdPartyRdo.setRadioButton();
    yesElectronicDeliveryRdo.setRadioButton();
//    stateSignedDropD.selectByText("Minnesota");
    stateSelect.sendKeys("Minnesota");
    nextbtn.click();
    return navigatingTo(TliQuoteExistingPage.class);
  }

  /**
   * Complete the Proposed Insured details for alternative path one
   *
   * @return the TliQuoteProposedInsuredPage  object
   */

  @Step("Complete TLI Proposed Insured Page Questions for AltPath1")
  public TliQuoteExistingPage completeProposedInsuredDetailsAltPathOne() {
    streetAddressInput.setText(streetAddress);
    streetAddressLineTwoInput.setText(streetAddressLineTwo);
    cityInput.setText(city);
    stateBornDropD.selectByText("Minnesota");
    socialSecurityInput.setText(socialSecurity);
    driversLicenseNumInput.setText(driversLicense);
    afterNoonRdo.setRadioButton();
    occupationInput.setText(occupation);
    annualIncomeInput.setText("200000");
    yesSecondIncomeRdo.setRadioButton();
    otherOccupationInput.setText(otherOcc);
    otherEarnedIncomeInput.setText(otherIncome);
    noMilitaryRdo.setRadioButton();
    noThirdPartyRdo.setRadioButton();
    yesElectronicDeliveryRdo.setRadioButton();
//    stateSignedDropD.selectByText("Minnesota");
    stateSelect.sendKeys("Minnesota");
    nextbtn.click();
    return navigatingTo(TliQuoteExistingPage.class);
  }

  /**
   * Complete the Proposed Insured details for alternative path four
   *
   * @return the TliQuoteProposedInsuredPage  object
   */

  @Step("Complete TLI Proposed Insured Page Questions for AltPath4")
  public TliQuoteExistingPage completeTLIQuoteEligibilityQuestionsAltPathFour() {
    streetAddressInput.setText(streetAddress);
    streetAddressLineTwoInput.setText(streetAddressLineTwo);
    cityInput.setText(city);
    stateBornDropD.selectByText("Minnesota");
    socialSecurityInput.setText(socialSecurity);
    driversLicenseNumInput.setText(driversLicense);
    afterNoonRdo.setRadioButton();
    occupationInput.setText(occupation);
    annualIncomeInput.setText("200000");
    noSecondIncomeRdo.setRadioButton();
    noMilitaryRdo.setRadioButton();
    noThirdPartyRdo.setRadioButton();
    yesElectronicDeliveryRdo.setRadioButton();
//    stateSignedDropD.selectByText("Minnesota");
    stateSelect.sendKeys("Minnesota");
    nextbtn.click();
    return navigatingTo(TliQuoteExistingPage.class);
  }

  /**
   * Enter the negative user details for the TLI Eligibility page Error Messages
   */

  @Step("Fill in the TLI Proposed Insured Page Yes Military")
  public TliQuoteProposedInsuredPage tliYesMilitaryFill() {
    yesMilitaryRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the TLI Proposed Insured Page Yes Third Party")
  public TliQuoteProposedInsuredPage tliYesThirdPartyFill() {
    yesThirdPartyRdo.setRadioButton();
    return this;
  }
}