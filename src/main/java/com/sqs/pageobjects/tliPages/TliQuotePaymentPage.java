package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.pageobjects.BrightpeakHome;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import com.sqs.web.elements.TextInput;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 28/03/2017.
 */
public class TliQuotePaymentPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput routingNumber = new TextInput(By.xpath("//input[@name='routingNumber']"));
  private final TextInput accountNumber = new TextInput(By.xpath("//input[@name='accountNumber']"));
  //Radio Buttons
  private final RadioButton yesPaymentResponsible = new RadioButton(By.xpath("//*[@name='responsible']/*[@aria-label='YES']"));
  private final RadioButton noPaymentResponsible = new RadioButton(By.xpath("//*[@name='responsible']/*[@aria-label='NO']"));
  private final RadioButton accountType = new RadioButton(By.xpath("//*[@value='checking']"));
  private final RadioButton reminderEmail = new RadioButton(By.xpath("//*[@name='reminderEmail']/*[@aria-label='YES']"));
  private final RadioButton dividendPayout = new RadioButton(By.xpath("//*[@value='CASH']"));
  private final RadioButton autoIncreaseOptOut = new RadioButton(By.xpath("//*[@aria-label='APPLICATION.PAYMENT.AUTO_OPT_OUT']"));
  //Buttons
  private final Button submitButton = new Button(By.xpath("//span[contains(text(), 'SUBMIT')]"));
  private final Button goBackToBrightpeak = new Button(By.xpath("//span[contains(text(), 'GO BACK TO BRIGHTPEAK')]"));

  public TliQuotePaymentPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Payment Page has successfully loaded.");
  }

  /**
   * Complete the payment confirmation details
   *
   * @return the Brightpeak Financial homepage
   */

  @Step("Complete TLI Payment Page Questions, Confirmation, and Return to Homepage")
  public BrightpeakHome completePaymentConfirmation() {
    String accountNum = "" + RandomStringUtils.randomNumeric(8);
    yesPaymentResponsible.setRadioButton();
    accountType.setRadioButton();
    routingNumber.setText("091000019");
    accountNumber.setText(accountNum);
    reminderEmail.setRadioButton();
    dividendPayout.setRadioButton();
    autoIncreaseOptOut.setRadioButton();
    submitButton.click();
    goBackToBrightpeak.click();
    return navigatingTo(BrightpeakHome.class);
  }

  /**
   * Enter the negative user details for the TLI Payment page Error Messages
   *
   * @return the TLI Payment Page page object
   */

  @Step("Fill in the TLI Preliminary Declaration Page Yes Past Six Months")
  public TliQuotePaymentPage tliNoPaymentResponsibleFill() {
    noPaymentResponsible.setRadioButton();
    return this;
  }

}
