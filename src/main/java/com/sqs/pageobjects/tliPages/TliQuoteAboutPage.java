package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.common.CommonActions;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import com.sqs.web.elements.TextInput;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteAboutPage.
 */
public class TliQuoteAboutPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput firstNameInput = new TextInput(By.id("firstname"));
  private final TextInput lastNameInput = new TextInput(By.id("lastname"));
  private final TextInput emailInput = new TextInput(By.id("email"));
  private final TextInput phoneNumberInput = new TextInput(By.xpath("//input[@name='phone']"));
  private final TextInput zipCodeInput = new TextInput(By.xpath("//input[@name='zip']"));
  private final TextInput dobInput = new TextInput(By.xpath("//input[@name='dob']"));
  private final TextInput feetInput = new TextInput(By.xpath("//input[@name='heightfeet']"));
  private final TextInput inchesInput = new TextInput(By.xpath("//input[@name='heightinches']"));
  private final TextInput poundsInput = new TextInput(By.xpath("//input[@name='weight']"));
  //RadioButtons
  private final RadioButton femaleRadio = new RadioButton(By.xpath("//*[@value='Female']"));
  private final RadioButton someQuickHealthQuestionsNoRadio = new RadioButton(By.xpath("//*[@name='tobacco']/*[@aria-label='NO']"));
  //Button
  private final Button next = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));
  //member variables
  private String firstName = "Testname-sqs-" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
  private String lastName = "Lastname" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
  private String email = "faith.chapman@test.brightpeakfinancial.com";
  private String phoneNumber = "612555" + RandomStringUtils.randomNumeric(4);
  private String zipCode = "55401";
  private String dateOfBirth = "01011980";
  private String feetHeight = "5";
  private String inchesHeight = "8";
  private String weight = "180";

  /**
   * Instantiates a new Brightpeak TliQuoteAboutPage.
   */
  public TliQuoteAboutPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote About Page has successfully loaded.");
  }

  /**
   * Enter the user details
   *
   * @return the TLI Quote About page objects
   */

  @Step("Complete TLI About Page Questions")
  public TliQuoteNeedsPage completeTLIAboutPageQuestions() {
    firstNameInput.setText(firstName);
    lastNameInput.setText(lastName);
    emailInput.setText(email);
    phoneNumberInput.setText(phoneNumber);
    zipCodeInput.setText(zipCode);
    dobInput.setText(dateOfBirth);
    femaleRadio.setRadioButton();
    someQuickHealthQuestionsNoRadio.setRadioButton();
    feetInput.setText(feetHeight);
    inchesInput.setText(inchesHeight);
    poundsInput.setText(weight);
    CommonActions.pauseTest(2);
    next.click();
    return navigatingTo(TliQuoteNeedsPage.class);
  }

  /**
   * Enter the negative user details for the TLI About page Error Messages
   *
   */

  @Step("Fill in the TLI About Page DOB field")
  public TliQuoteAboutPage tliDOBFill(String dob) {
    dobInput.setText(dob);
    return this;
  }

  @Step("Fill in the TLI About Page Feet field")
  public TliQuoteAboutPage tliFeetFill(String feet) {
    feetInput.setText(feet);
    return this;
  }

  @Step("Fill in the TLI About Page Inches field")
  public TliQuoteAboutPage tliInchesFill(String inches) {
    inchesInput.setText(inches);
    return this;
  }

  @Step("Fill in the TLI About Page Pounds field")
  public TliQuoteAboutPage tliPoundsFill(String pounds) {
    poundsInput.setText(pounds);
    return this;
  }


}
