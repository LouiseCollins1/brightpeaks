package com.sqs.pageobjects.tliPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteNeedsPage.
 */
public class TliQuoteNeedsPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton yesRadio = new RadioButton(By.xpath("//*[@name='userKnowsNeeds']/*[@aria-label='YES']"));
  private final RadioButton noRadio = new RadioButton(By.xpath("//*[@name='userKnowsNeeds']/*[@aria-label='NO']"));
  //TextInput
  private final TextInput coverageAmount = new TextInput(By.xpath("//input[@name='coverageAmount']"));
  private final TextInput annualEarned = new TextInput(By.xpath("//input[@name='annualEarnedIncome']"));
  //Dropdown
  private final Dropdown policyLengthInput = new Dropdown("Desired Policy Length");
  //Button
  private final Button goodCoverage = new Button(By.xpath(".//*[@id='needs-coverage']//div[contains(text(),'GOOD')]"));
  private final Button betterCoverageBtn = new Button(By.xpath("//div[contains(text(),'BETTER')]"));
  //  private final Button next = new Button(By.xpath(".//*[@id='needs-coverage']//div[contains(text(),'BEST')]"));

  private final Button twentyYearTermBtn = new Button(By.xpath("//div[contains(text(),'20 year')]"));
  private final Button next = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  /**
   * Instantiates a new Brightpeak TliQuoteNeedsPage.
   */
  public TliQuoteNeedsPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Needs Page has successfully loaded.");
  }

  /**
   * Complete the TLI Quotes Needs Page questions
   *
   * @return the tli quotes results page page object
   */
  @Step("Complete TLI Needs Page Questions")
  public TliQuoteResultsPage completeQuoteNeedsQuestions() {
    yesRadio.setRadioButton();
    coverageAmount.setText("100000");
    policyLengthInput.selectByText("20 year");
    next.click();
    return navigatingTo(TliQuoteResultsPage.class);
  }

  /**
   * Enter the negative user details for the TLI Needs page Error Messages
   */
  @Step("Fill in the TLI Needs Page coverage field")
  public TliQuoteNeedsPage tliCoverageFill(String coverage) {
    yesRadio.setRadioButton();
    coverageAmount.setText(coverage);
    return this;
  }

  /**
   * Complete the TLI Quote Needs page questions for TLI Alt Path one
   *
   * @return the TLI Quote Results Page page object
   */
  @Step("Complete TLI Quote Needs Questions for Alt Path 1")
  public TliQuoteResultsPage completeQuoteNeedsQuestionsAltPath1() {
    noRadio.setRadioButton();
    annualEarned.setText("200000");
    betterCoverageBtn.click();
    twentyYearTermBtn.click();
    next.click();
    return navigatingTo(TliQuoteResultsPage.class);
  }

}