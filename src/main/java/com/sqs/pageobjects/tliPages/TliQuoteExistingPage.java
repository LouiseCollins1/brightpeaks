package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteExistingPage.
 */

public class TliQuoteExistingPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton noExistingThriventInsuranceRadio = new RadioButton(By.xpath("//*[@name='thriventExistingInsurance']/*[@aria-label='NO']"));
  private final RadioButton noExistingOtherInsuranceRadio = new RadioButton(By.xpath("//*[@name='otherExistingInsurance']/*[@aria-label='NO']"));
  //Button
  private final Button next = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public TliQuoteExistingPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Existing Page has successfully loaded.");
  }

  /**
   * Complete the TliQuoteExistingPage details
   *
   * @return the TliQuotePrelimDeclarationPage object
   */

  @Step("Complete TLI Existing Page Questions")
  public TliQuotePrelimDeclarationPage completeExistingInsuranceDetails() {
    noExistingThriventInsuranceRadio.click();
    noExistingOtherInsuranceRadio.click();
    next.click();
    return navigatingTo(TliQuotePrelimDeclarationPage.class);
  }
}
