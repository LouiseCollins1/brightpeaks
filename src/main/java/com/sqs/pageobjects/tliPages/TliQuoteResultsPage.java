package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteResultsPage.
 */
public class TliQuoteResultsPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton yesRadio = new RadioButton(By.xpath("//*[@name='premiumWaiver']/*[@value='true']"));
  private final RadioButton noRadio = new RadioButton(By.xpath("//*[@name='premiumWaiver']/*[@value='false']"));
  //Button
  private final Button applyOnlineButton = new Button(By.xpath("//button[contains(text(), 'APPLY ONLINE')]"));
  private final Button minusBtn = new Button(By.xpath("//*[contains(text(),'remove_circle')]"));
  private final Button addBtn = new Button(By.xpath("//*[contains(text(),'add_circle')]"));
  private final Button sliderBtn = new Button(By.xpath("//div[@class='md-thumb']"));


  /**
   * Instantiates a new Brightpeak TliQuoteResultsPage.
   */

  public TliQuoteResultsPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Results Page has successfully loaded.");
  }

  /**
   * Click on the Apply Online button
   *
   * @return the TliQuoteEligibilityPage object
   */
  @Step("Complete TLI Results Page Questions")
  public TliQuoteEligibilityPage completeResultsPageApplyOnline() {
    noRadio.setRadioButton();
    applyOnlineButton.click();
    return navigatingTo(TliQuoteEligibilityPage.class);
  }

  /**
   * Click on the Minus button for coverage adjustment
   *
   * @return the TliQuoteEligibilityPage object
   */
  @Step("Click on Minus Button Twice")
  public TliQuoteEligibilityPage clickOnMinusAddAndAdjustCover() {
    int minusCount = 0;
    do {
      minusBtn.click();
      minusCount++;
    }
    while (minusCount < 2);

    int addCount = 0;
    do {
      addBtn.click();
      addCount++;
    }
    while (addCount < 4);

    //TODO need slider method.
//    sliderBtn.click();
    applyOnlineButton.click();
    return navigatingTo(TliQuoteEligibilityPage.class);

  }
}