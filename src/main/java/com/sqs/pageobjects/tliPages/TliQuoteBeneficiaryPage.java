package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import com.sqs.web.elements.TextInput;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TliQuoteBeneficiaryPage.
 */

public class TliQuoteBeneficiaryPage extends PageObject {

  //Links
  private static final String URL = "https://test.brightpeakfinancial.com/product/term-quote/#/quote/beneficiary";
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput spouseFirstNameInput = new TextInput(By.xpath("//input[@name='firstName']"));
  //RadioButtons
  private final RadioButton noAddBeneficiaryRadio = new RadioButton(By.xpath("//*[@name='addBene']/*[@aria-label='NO']"));
  private final RadioButton noContingentBeneficaryRadio = new RadioButton(By.xpath("//*[@name='addContingent']/*[@aria-label='NO']"));
  //Button
  private final Button next = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public TliQuoteBeneficiaryPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("TLI Quote Beneficiary Page has successfully loaded.");
  }

  /**
   * Complete the TliQuoteBeneficiaryPage details
   *
   * @return the TliQuotePaymentPage page object
   */

  @Step("Complete TLI Beneficiary Page Questions")
  public TliQuotePaymentPage completeBeneficiaryDetails() {
    spouseFirstNameInput.setText("John");
    noAddBeneficiaryRadio.setRadioButton();
    noContingentBeneficaryRadio.setRadioButton();
    next.click();
    return navigatingTo(TliQuotePaymentPage.class);
  }

}