package com.sqs.pageobjects.tliPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak TLIpage.
 */
public class TermLifeInsurancePage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  private final Hyperlink getMyFreeTLIQuoteLink = new Hyperlink(By.id("btn-term-life-insurance"));

  /**
   * Instantiates a new Brightpeak TLIPage.
   */
  public TermLifeInsurancePage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("Term Life Insurance Page has successfully loaded.");
  }

  /**
   * click on the Get Me Free Quote link.
   *
   * @return to questionnaire for TLI
   */

  @Step("Navigate from Term Life Insurance to Quote")
  public TliQuoteAboutPage clickGetMyFreeQuoteTLILink() {
    getMyFreeTLIQuoteLink.click();
    return navigatingTo(TliQuoteAboutPage.class);
  }

  /**
   * Method to complete pages up to the TLI Quote Eligibility page.
   *
   * @return the TLI Quote Eligibility Page object
   */
  public TliQuoteEligibilityPage completeTliAboutQuoteNeedsResultsPages() {
    clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline();
    return navigatingTo(TliQuoteEligibilityPage.class);
  }

  /**
   * Method to complete pages up to the TLI Quote Proposed Insured page.
   *
   * @return the TLI Quote Proposed Insured Page object
   */
  public TliQuoteProposedInsuredPage completeTliAboutQuoteNeedsResultsEligibilityPages() {
    clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestions();
    return navigatingTo(TliQuoteProposedInsuredPage.class);
  }

  /**
   * Method to complete pages up to the TLI Quote Preliminary Declaration page.
   *
   * @return the TLI Quote Preliminary Declaration Page object
   */
  public TliQuotePrelimDeclarationPage completeTliAboutQuoteNeedsResultsEligibilityPrelimPages() {
    clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestions()
        .completeProposedInsuredDetails()
        .completeExistingInsuranceDetails();
    return navigatingTo(TliQuotePrelimDeclarationPage.class);
  }

  /**
   * Method to complete pages up to the TLI Quote Preliminary Declaration page.
   *
   * @return the TLI Quote Preliminary Declaration Page object
   */
  public TliQuotePaymentPage completeTliAboutQuoteNeedsResultsEligibilityPrelimPaymentPages() {
    clickGetMyFreeQuoteTLILink()
        .completeTLIAboutPageQuestions()
        .completeQuoteNeedsQuestions()
        .completeResultsPageApplyOnline()
        .completeTLIQuoteEligibilityQuestions()
        .completeProposedInsuredDetails()
        .completeExistingInsuranceDetails()
        .completeDeclarationDetails()
        .completeBeneficiaryDetails();
    return navigatingTo(TliQuotePaymentPage.class);
  }

}