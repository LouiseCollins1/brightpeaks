package com.sqs.pageobjects.diPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DisabilityInsurancePage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  private final Hyperlink getMyFreeDIQuoteLink = new Hyperlink(By.id("btn-disability-income-insurance"));

  /**
   * Instantiates a new Brightpeak DIPage.
   */
  public DisabilityInsurancePage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("Disability Insurance Page has successfully loaded.");
  }
  /**
   * click on the Get Me Free Quote link
   *
   * @return to questionnaire for DI
   */

  @Step("Navigate from Disability Insurance page to free quote")
  public DiQuoteQuickQuotePage clickGetMyFreeQuoteLink() {
    getMyFreeDIQuoteLink.click();
    return navigatingTo(DiQuoteQuickQuotePage.class);
  }

  /**
   * Method to complete pages up to the DI Quote Eligibility page.
   *
   * @return the DI Quote Eligibility Page object
   */
  public DiQuoteEligibilityPage completeUpToEligibility(){
    clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion();
    return navigatingTo(DiQuoteEligibilityPage.class);
  }

  /**
   * Method to complete pages up to the DI Quote Employment page.
   *
   * @return the DI Quote Employment Page object
   */
  public DiQuoteEmploymentPage completeUpToEmployment(){
    clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo();
    return navigatingTo(DiQuoteEmploymentPage.class);
  }

  /**
   * Method to complete pages up to the DI Quote Preliminary Declaration page.
   *
   * @return the DI Quote Preliminary Declaration Page object
   */
  public DiQuoteDeclarationPage completeUpToPrelimDeclaration(){
    clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo()
        .completeDIEmploymentQuestion()
        .completeDIExistingQuestion();
    return navigatingTo(DiQuoteDeclarationPage.class);
  }

  /**
   * Method to complete pages up to the DI Quote Payment page.
   *
   * @return the DI Quote Payment Page object
   */
  public DiQuotePaymentPage completeUpToPayment(){
    clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo()
        .completeDIEmploymentQuestion()
        .completeDIExistingQuestion()
        .completeDIDeclarationQuestion();
    return navigatingTo(DiQuotePaymentPage.class);
  }

  /**
   * Method to complete pages up to the DI Quote Submission page.
   *
   * @return the DI Quote Submission Page object
   */
  public DiQuoteSubmissionPage completeUpToSubmission(){
    clickGetMyFreeQuoteLink()
        .completeDIQuickQuote()
        .completeDIAboutPageQuestions()
        .completeDIOccupationQuestion()
        .completeDICoverageQuestion()
        .completeDIResultsQuestion()
        .completeDIQuoteEligibilityQuestions()
        .completeDIPersonalInfo()
        .completeDIEmploymentQuestion()
        .completeDIExistingQuestion()
        .completeDIDeclarationQuestion()
        .completeDIPaymentQuestion();
    return navigatingTo(DiQuoteSubmissionPage.class);
  }


}