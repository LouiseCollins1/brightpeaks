package com.sqs.pageobjects.diPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.pageobjects.BrightpeakHome;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteSubmissionPage extends PageObject {

  //Member Variables
  private String city = "Minneapolis";
  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput signedCityInput = new TextInput(By.xpath(".//*[@name='city']"));
  //Selects
  private final Dropdown signedStateInput = new Dropdown("State");
  //workaround for 'State' dropdown
  private final Select signedStateSelect = new Select(By.xpath("//*[@name='state']"));
  private final Select signedStateValueSelect = new Select(By.xpath("//md-option[@name='MN']"));
  //RadioButtons
  private final RadioButton yesElectronicSubRdo = new RadioButton(By.xpath(".//*[@name='electronicsub']/*[@aria-label='YES']"));
  private final RadioButton noElectronicSubRdo = new RadioButton(By.xpath(".//*[@name='electronicsub']/*[@aria-label='NO']"));
  //Button
  private final Button submitBtn = new Button(By.xpath("//span[contains(text(), 'SUBMIT')]"));
  private final Button goBackToBrightpeakBtn = new Button(By.xpath("//span[contains(text(), 'Go back to brightpeak')]"));

  public DiQuoteSubmissionPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Submission Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteSubmissionPage details
   *
   * @return the DiQuoteSubmissionPage object
   */

  @Step("Complete DI Submission Page Questions")
  public BrightpeakHome completeDISubmissionQuestions() {
    signedCityInput.setText(city);
    signedStateInput.selectByText("Minnesota");
//        signedStateSelect.click();
//        signedStateValueSelect.click();
    yesElectronicSubRdo.setRadioButton();
    submitBtn.click();
    goBackToBrightpeakBtn.click();
    return navigatingTo(BrightpeakHome.class);
  }

  /**Enter the negative user details for the DI Payment page Error Messages
   *
   *
   */

  @Step("Fill in the DI Submission Page Signed State")
  public DiQuoteSubmissionPage diSignedStateErrorFill(String state) {
    signedStateInput.selectByText(state);
    return this;
  }

  @Step("Fill in the DI Submission Page Electronic Delivery")
  public DiQuoteSubmissionPage diElectronicDeliveryFill() {
    noElectronicSubRdo.setRadioButton();
    return this;
  }

}