package com.sqs.pageobjects.diPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteOccupationPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton whiteCollarRdo = new RadioButton(By.xpath("//b[contains(text(),'White Collar')]"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuoteOccupationPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Occupation Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteOccupationPage details
   *
   * @return the DiQuoteOccupationPage object
   */

  @Step("Complete DI Occupation Page Questions")
  public DiQuoteCoveragePage completeDIOccupationQuestion() {
    whiteCollarRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuoteCoveragePage.class);
  }
}