package com.sqs.pageobjects.diPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteDeclarationPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton afterFiveContactTimeRdo = new RadioButton(By.xpath(".//*[@name='timeToCall']/*[@value='NIGHT']"));
  private final RadioButton noDiagnosedRdo = new RadioButton(By.xpath("//*[@name='diagnosed']/*[@aria-label='NO']"));
  private final RadioButton yesDiagnosedRdo = new RadioButton(By.xpath("//*[@name='diagnosed']/*[@aria-label='YES']"));
  private final RadioButton noAdvisedRdo = new RadioButton(By.xpath("//*[@name='advised']/*[@aria-label='NO']"));
  private final RadioButton yesAdvisedRdo = new RadioButton(By.xpath("//*[@name='advised']/*[@aria-label='YES']"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuoteDeclarationPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Declaration Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteDeclarationPage details
   *
   * @return the DiQuoteDeclarationPage object
   */

  @Step("Complete DI Declaration Page Questions")
  public DiQuotePaymentPage completeDIDeclarationQuestion() {
    afterFiveContactTimeRdo.setRadioButton();
    noDiagnosedRdo.setRadioButton();
    noAdvisedRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuotePaymentPage.class);
  }

  /**Enter the negative user details for the DI Preliminary Declaration page Error Messages
   *
   *
   */

  @Step("Fill in the DI Declaration Page Yes Diagnosed")
  public DiQuoteDeclarationPage diYesDiagnosedFill() {
    yesDiagnosedRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Declaration Page Yes Been Advised test/surgery")
  public DiQuoteDeclarationPage diYesAdvisedFill() {
    yesAdvisedRdo.setRadioButton();
    return this;
  }

}
