package com.sqs.pageobjects.diPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuotePersonalInfoPage extends PageObject {

  //Member Variables
  private String streetAddress = "123 Oak St.";
  private String streetAddressLineTwo = "#100";
  private String city = "Minneapolis";
  private String socialSecurity = "476" + RandomStringUtils.randomNumeric(6);
  private String driversLicense = "C" + RandomStringUtils.randomNumeric(11);
  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //TextInput
  private final TextInput streetAddressInput = new TextInput(By.xpath(".//*[@name='address1']"));
  private final TextInput streetAddressLineTwoInput = new TextInput(By.xpath(".//*[@name='address2']"));
  private final TextInput cityInput = new TextInput(By.xpath(".//input[@name='city']"));
  private final TextInput socialSecurityInput = new TextInput(By.xpath(".//*[@name='ssn']"));
  private final TextInput driversLicenseNumber = new TextInput(By.xpath(".//*[@name='driversLicense']"));
  //Dropdown
  private final Dropdown stateBornDropdown = new Dropdown("State where you were born");
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuotePersonalInfoPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Personal Information Page has successfully loaded.");
  }

  /**
   * Complete the Personal Information details
   *
   * @return the DiQuotePersonalInfoPage object
   */

  @Step("Complete DI Personal Information Page Questions")
  public DiQuoteEmploymentPage completeDIPersonalInfo() {
    streetAddressInput.setText(streetAddress);
    streetAddressLineTwoInput.setText(streetAddressLineTwo);
    cityInput.setText(city);
    stateBornDropdown.selectByText("Minnesota");
    socialSecurityInput.setText(socialSecurity);
    driversLicenseNumber.setText(driversLicense);
    nextBtn.click();
    return navigatingTo(DiQuoteEmploymentPage.class);
  }

}