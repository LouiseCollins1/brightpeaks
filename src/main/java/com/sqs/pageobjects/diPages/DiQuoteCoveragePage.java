package com.sqs.pageobjects.diPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteCoveragePage extends PageObject {

  //Member Variables
  private String grossIncome = "100000";
  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //Input
  private final TextInput grossIncomeInput = new TextInput(By.xpath("//input[@name='grossIncome']"));
  private final TextInput recommendedCoverageInput = new TextInput(By.xpath("//*[@role='slider']"));
  //RadioButtons
  private final Dropdown eliminationInput = new Dropdown("30 Days");
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuoteCoveragePage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Coverage Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteCoveragePage details
   *
   * @return the DiQuoteCoveragePage object
   */

  @Step("Complete the DI Coverage Insurance Page")
  public DiQuoteResultsPage completeDICoverageQuestion() {
    grossIncomeInput.setText(grossIncome);
    eliminationInput.selectByText("60 Days");
    nextBtn.click();
    return navigatingTo(DiQuoteResultsPage.class);
  }

  /**Enter the negative user details for the DI Coverage page Error Messages
   *
   *
   */

  @Step("Fill in the TLI Needs Page coverage field")
  public DiQuoteCoveragePage diCoverageFill(String coverage) {
    grossIncomeInput.setText(coverage);
    return this;
  }

}