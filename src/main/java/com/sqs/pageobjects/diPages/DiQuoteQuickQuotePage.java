package com.sqs.pageobjects.diPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteQuickQuotePage extends PageObject {

  //Member Variables
  private String income = "38000";
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //Dropdown
  private final Dropdown genderDropdown = new Dropdown("Male");
  private final Dropdown bornYearDropdown = new Dropdown("1985");
  private final Dropdown industryDropdown = new Dropdown("Management");
  //Text Input
  private final TextInput monthlyPaymentInput = new TextInput(By.xpath(".//input[@formcontrolname='income']"));
  //Label
  private final Button paymentDetailsDropdown = new Button(By.xpath("//a[contains(text(), 'Monthly Payment Details')]"));
  //Radio Buttons
  private final RadioButton sixtyDaysEliminationRdo = new RadioButton(By.xpath("//*[@formcontrolname='eliminationPeriod']/*[@value='60']"));
  private final RadioButton fiveYearsBenefitRdo = new RadioButton(By.xpath("//*[contains(text(),'5 Years')]"));
  private final Button getStartedBtn = new Button(By.xpath("//button[@class='mat-raised-button']"));

  public DiQuoteQuickQuotePage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Quick Quote Page has successfully loaded.");
  }

  /**
   * Complete the Quick Quote details
   *
   * @return the DiQuoteQuickQuotePage object
   */

  @Step("Complete DI Quick Quote Page Questions")
  public DiQuoteAboutPage completeDIQuickQuote() {
    genderDropdown.selectByText("Female");
    bornYearDropdown.selectByText("1993");
    industryDropdown.selectByText("Light Labor");
    monthlyPaymentInput.clearText();
    monthlyPaymentInput.setText(income);
    paymentDetailsDropdown.click();
    sixtyDaysEliminationRdo.setRadioButton();
    fiveYearsBenefitRdo.setRadioButton();
    getStartedBtn.click();
    return navigatingTo(DiQuoteAboutPage.class);
  }
}

