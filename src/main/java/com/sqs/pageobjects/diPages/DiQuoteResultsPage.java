package com.sqs.pageobjects.diPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteResultsPage extends PageObject {

  //Member Variables
  private String term = "5 years";
  private String elimPeriod = "60 Days"; //value 1 represents 60Days
  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //Input
//    private final TextInput cashBenefitTermInput = new TextInput(By.xpath("//*[@aria-label='term']//div[@class='md-thumb']"));
  private final TextInput cashBenefitTermInput = new TextInput(By.xpath("//*[@aria-label='term']"));
  private final TextInput elimPeriodInput = new TextInput(By.xpath("//*[@aria-label='elim-period']//div[@class='md-thumb']"));
  //Button
  private final Button applyOnlineBtn = new Button(By.xpath("//div[contains(text(), 'APPLY ONLINE')]"));

  public DiQuoteResultsPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Results Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteResultsPage details
   *
   * @return the DiQuoteResultsPage object
   */

  @Step("Complete the DI Results Insurance Page")
  public DiQuoteEligibilityPage completeDIResultsQuestion() {
    //TODO need to get method for slider
//    cashBenefitTermInput.sendKeys(term);
//        elimPeriodInput.setText(elimPeriod);
    applyOnlineBtn.click();
    return navigatingTo(DiQuoteEligibilityPage.class);
  }
}