package com.sqs.pageobjects.diPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteExistingPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton noOtherDisabilityRdo = new RadioButton(By.xpath("//*[@name='otherDisability']/*[@aria-label='NO']"));
  private final RadioButton yesOtherDisabilityRdo = new RadioButton(By.xpath("//*[@name='otherDisability']/*[@aria-label='YES']"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuoteExistingPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Existing Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteExistingPage details
   *
   * @return the DiQuoteExistingPage object
   */

  @Step("Complete DI Existing Page Questions")
  public DiQuoteDeclarationPage completeDIExistingQuestion() {
    noOtherDisabilityRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuoteDeclarationPage.class);
  }

  /**Enter the negative user details for the DI Existing page Error Messages
   *
   *
   */

  @Step("Fill in the DI Existing Page Yes Existing Other Disability Income")
  public DiQuoteExistingPage diYesExistingOtherDisabilityIncomeFill() {
    yesOtherDisabilityRdo.setRadioButton();
    return this;
  }
}
