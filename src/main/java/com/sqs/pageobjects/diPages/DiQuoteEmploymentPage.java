package com.sqs.pageobjects.diPages;

import com.sqs.common.Dropdown;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.*;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteEmploymentPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton noSelfEmployedRdo = new RadioButton(By.xpath("//*[@name='selfEmployeed']/*[@aria-label='NO']"));
  private final RadioButton yesSelfEmployedRdo = new RadioButton(By.xpath("//*[@name='selfEmployeed']/*[@aria-label='YES']"));
  private final RadioButton noSecondIncomeRdo = new RadioButton(By.xpath("//*[@name='secondIncome']/*[@aria-label='NO']"));
  private final RadioButton yesSecondIncomeRdo = new RadioButton(By.xpath("//*[@name='secondIncome']/*[@aria-label='YES']"));
  private final RadioButton yesContributeSSRdo = new RadioButton(By.xpath("//*[@name='contributingSS']/*[@aria-label='YES']"));
  private final RadioButton noContributeSSRdo = new RadioButton(By.xpath("//*[@name='contributingSS']/*[@aria-label='NO']"));
  //TextInput
  private final TextInput currentEmployerInput = new TextInput(By.xpath("//input[@name='currentEmployer']"));
  private final TextInput employerCityInput = new TextInput(By.xpath("//input[@name='employerCity']"));
  private final TextInput currentOccupationInput = new TextInput(By.xpath("//input[@name='currentOccupation']"));
  private final TextInput dutiesInput = new TextInput(By.xpath("//input[@name='duties']"));
  private final TextInput yearsInput = new TextInput(By.xpath("//input[@name='years']"));
  private final TextInput monthsInput = new TextInput(By.xpath("//input[@name='months']"));
  private final TextInput avgHoursInput = new TextInput(By.xpath("//input[@name='avgHours']"));
  private final TextInput explainContributingSSNoInput = new TextInput(By.xpath("//input[@name='contributingSSIfNo']"));
  //Selects
  private final Dropdown employerStateInput = new Dropdown("state");
  //workaround for 'State' dropdown
  private final Select signedStateSelect = new Select(By.xpath("//*[@name='addressState']"));
  private final Select signedStateValueSelect = new Select(By.xpath("//md-option[@name='MN']"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));
  //Member Variables
  private String currentEmployer = "XYZ Company";
  private String cityInput = "Minneapolis";
  private String currentOccupation = "IT Consultant";
  private String duties = "Testing Software";
  private String years = "5";
  private String months = "2";
  private String avgHours = "40";
  private String explainNotContributingSS = "This is my explanation";

  public DiQuoteEmploymentPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Employment Page has successfully loaded.");
  }

  /**
   * Complete the DiQuoteEmploymentPage details.
   *
   * @return the DiQuoteEmploymentPage object
   */

  @Step("Complete DI Employment Page Questions")
  public DiQuoteExistingPage completeDIEmploymentQuestion() {
    noSelfEmployedRdo.setRadioButton();
    noSecondIncomeRdo.setRadioButton();
    yesContributeSSRdo.setRadioButton();
    currentEmployerInput.setText(currentEmployer);
    employerCityInput.setText(cityInput);
//        employerStateInput.selectByText("Minnesota");
    signedStateSelect.click();
    signedStateValueSelect.click();
    currentOccupationInput.setText(currentOccupation);
    dutiesInput.setText(duties);
    yearsInput.setText(years);
    monthsInput.setText(months);
    avgHoursInput.setText(avgHours);
    nextBtn.click();
    return navigatingTo(DiQuoteExistingPage.class);
  }

  /**
   * Complete the DiQuoteEmploymentPage details for alternative path three.
   *
   * @return the DiQuoteEmploymentPage page object
   */

  @Step("Complete DI Employment Page Questions for AltPath3")
  public DiQuoteExistingPage completeDIQuoteEligibilityQuestionsAltPathThree() {
    noSelfEmployedRdo.setRadioButton();
    noSecondIncomeRdo.setRadioButton();
    noContributeSSRdo.setRadioButton();
    explainContributingSSNoInput.setText(explainNotContributingSS);
    currentEmployerInput.setText(currentEmployer);
    employerCityInput.setText(cityInput);
//        employerStateInput.selectByText("Minnesota");
    signedStateSelect.click();
    signedStateValueSelect.click();
    currentOccupationInput.setText(currentOccupation);
    dutiesInput.setText(duties);
    yearsInput.setText(years);
    monthsInput.setText(months);
    avgHoursInput.setText(avgHours);
    nextBtn.click();
    return navigatingTo(DiQuoteExistingPage.class);
  }

  /**
   * Enter the negative user details for the DI Employment page Error Messages
   */

  @Step("Fill in the DI Employment Page Yes Self Employed")
  public DiQuoteEmploymentPage diYesSelfEmployedFill() {
    yesSelfEmployedRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Employment Page Yes Second Income")
  public DiQuoteEmploymentPage diYesSecondIncomeFill() {
    noSecondIncomeRdo.setRadioButton();
    yesSecondIncomeRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Employment Page Max months Worked")
  public DiQuoteEmploymentPage diMonthsWorkedFill(String months) {
    noSecondIncomeRdo.setRadioButton();
    monthsInput.setText(months);
    return this;
  }
}