package com.sqs.pageobjects.diPages;

import com.sqs.common.Keywords;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.RadioButton;
import com.sqs.web.elements.TextInput;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuotePaymentPage extends PageObject {

  //Member Variables
  private String accNum = "456789";
  private String routingAndTrackingNum = "091000019";
  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton yesApplicationPayRdo = new RadioButton(By.xpath("//*[@name='agreement']/*[@value='true']"));
  private final RadioButton noApplicationPayRdo = new RadioButton(By.xpath("//*[@name='agreement']/*[@value='false']"));
  private final RadioButton yesResponsibleForPayRdo = new RadioButton(By.xpath("//*[@name='responsible']/*[@value='true']"));
  private final RadioButton noResponsibleForPayRdo = new RadioButton(By.xpath("//*[@name='responsible']/*[@value='false']"));
  private final RadioButton accountTypeRdo = new RadioButton(By.xpath("//*[@name='accountTypeRdo']/*[@value='checking']"));
  private final RadioButton yesPaymentReminderRdo = new RadioButton(By.xpath("//*[@name='reminderEmail']/*[@value='true']"));
  private final RadioButton dividendPayoutCashRdo = new RadioButton(By.xpath("//*[@name='dividendPayout']/*[@value='CASH']"));
  private final RadioButton autoIncreaseOptOutRdo = new RadioButton(By.xpath("//*[@name='autoIncreaseOptOut']/*[@ng-value='true']"));
  //TextInput
  private final TextInput accNumberInput = new TextInput(By.xpath(".//*[@name='accountNumber']"));
  private final TextInput routingAndTransitInput = new TextInput(By.xpath(".//*[@name='routingNumber']"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  public DiQuotePaymentPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Payment Page has successfully loaded.");
  }

  /**
   * Complete the DiQuotePaymentPage details
   *
   * @return the DiQuotePaymentPage object
   */

  @Step("Complete DI Payment Page Questions")
  public DiQuoteSubmissionPage completeDIPaymentQuestion() {

    yesApplicationPayRdo.setRadioButton();
    yesResponsibleForPayRdo.setRadioButton();
    accountTypeRdo.setRadioButton();
    accNumberInput.setText(accNum);
    routingAndTransitInput.setText(routingAndTrackingNum);
    yesPaymentReminderRdo.setRadioButton();
    dividendPayoutCashRdo.setRadioButton();
    autoIncreaseOptOutRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuoteSubmissionPage.class);
  }

  @Step("Complete DI Payment Page Error Messages")
  public DiQuoteSubmissionPage diPaymentPageErrorMessages() {
    noApplicationPayRdo.setRadioButton();
    assert (getErrorMessageLabelElement(Keywords.Errors.DIPages.applicationPaymentError)).isPresent();
    yesApplicationPayRdo.setRadioButton();
    noResponsibleForPayRdo.setRadioButton();
    assert (getErrorMessageLabelElement(Keywords.Errors.DIPages.responsiblePaymentError)).isPresent();
    yesResponsibleForPayRdo.setRadioButton();
    accountTypeRdo.setRadioButton();
    accNumberInput.setText(accNum);
    routingAndTransitInput.setText("funny");
    assert (getErrorMessageLabelElement(Keywords.Errors.DIPages.routingError)).isPresent();
    routingAndTransitInput.clear();
    routingAndTransitInput.setText(routingAndTrackingNum);
    yesPaymentReminderRdo.setRadioButton();
    dividendPayoutCashRdo.setRadioButton();
    autoIncreaseOptOutRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuoteSubmissionPage.class);
  }

  /**Enter the negative user details for the DI Payment page Error Messages
   *
   *
   */

  @Step("Fill in the DI Payment Page No Application Payment")
  public DiQuotePaymentPage diNoApplicationPaymentFill() {
    noApplicationPayRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Payment Page No Responsible Payment")
  public DiQuotePaymentPage diNoResponsiblePaymentFill() {
    noResponsibleForPayRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Payment Page Routing Number")
  public DiQuotePaymentPage diRoutingNoFill(String routing) {
    routingAndTransitInput.setText(routing);
    return this;
  }
}