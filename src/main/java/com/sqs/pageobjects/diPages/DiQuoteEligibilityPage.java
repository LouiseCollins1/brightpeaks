package com.sqs.pageobjects.diPages;

import com.sqs.common.Keywords;
import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.RadioButton;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class DiQuoteEligibilityPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //RadioButtons
  private final RadioButton yesChristianRdo = new RadioButton(By.xpath("//*[@id='christian']/*[@aria-label='YES']"));
  private final RadioButton yesChristianSpouseRdo = new RadioButton(By.xpath("//*[@id='spouse']/*[@aria-label='YES']"));
  private final RadioButton yesUSCitizenRdo = new RadioButton(By.xpath("//*[@id='citizen']/*[@aria-label='YES']"));
  private final RadioButton noChristianRdo = new RadioButton(By.xpath("//*[@id='christian']/*[@aria-label='NO']"));
  private final RadioButton noChristianSpouseRdo = new RadioButton(By.xpath("//*[@id='spouse']/*[@aria-label='NO']"));
  private final RadioButton noUSCitizenRdo = new RadioButton(By.xpath("//*[@id='citizen']/*[@aria-label='NO']"));
  private final RadioButton noMilitaryRdo = new RadioButton(By.xpath("//*[@id='military']/*[@aria-label='NO']"));
  private final RadioButton noDisabilityRdo = new RadioButton(By.xpath("//*[@id='disability']/*[@aria-label='NO']"));
  private final RadioButton noAIDSRdo = new RadioButton(By.xpath("//*[@id='aids']/*[@aria-label='NO']"));
  private final RadioButton noTestingRdo = new RadioButton(By.xpath("//*[@id='testing']/*[@aria-label='NO']"));
  private final RadioButton noScheduledRdo = new RadioButton(By.xpath("//*[@id='scheduled']/*[@aria-label='NO']"));
  private final RadioButton noDrugRdo = new RadioButton(By.xpath("//*[@id='drug']/*[@aria-label='NO']"));
  private final RadioButton noTreatmentRdo = new RadioButton(By.xpath("//*[@id='treatment']/*[@aria-label='NO']"));
  private final RadioButton yesMilitaryRdo = new RadioButton(By.xpath("//*[@id='military']/*[@aria-label='YES']"));
  private final RadioButton yesDisabilityRdo = new RadioButton(By.xpath("//*[@id='disability']/*[@aria-label='YES']"));
  private final RadioButton yesAIDSRdo = new RadioButton(By.xpath("//*[@id='aids']/*[@aria-label='YES']"));
  private final RadioButton yesTestingRdo = new RadioButton(By.xpath("//*[@id='testing']/*[@aria-label='YES']"));
  private final RadioButton yesScheduledRdo = new RadioButton(By.xpath("//*[@id='scheduled']/*[@aria-label='YES']"));
  private final RadioButton yesDrugRdo = new RadioButton(By.xpath("//*[@id='drug']/*[@aria-label='YES']"));
  private final RadioButton yesTreatmentRdo = new RadioButton(By.xpath("//*[@id='treatment']/*[@aria-label='YES']"));
  private final Label youAreEligibleLabel = new Label(By.xpath("//div[contains(text(),'You are eligible!'"));
  //Button
  private final Button nextBtn = new Button(By.xpath("//span[contains(text(), 'NEXT')]"));

  /**
   * Instantiates a new Brightpeak DiQuoteEligibilityPage.
   */

  public DiQuoteEligibilityPage() {

  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("DI Quote Eligibility Page has successfully loaded.");
  }

  /**
   * Complete the DIQuoteEligibility details for the user.
   *
   * @return the DIQuoteEligibility page object
   */

  @Step("Complete DI Eligibility Page Questions")
  public DiQuotePersonalInfoPage completeDIQuoteEligibilityQuestions() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noMilitaryRdo.setRadioButton();
    noDisabilityRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noTestingRdo.setRadioButton();
    noScheduledRdo.setRadioButton();
    noDrugRdo.setRadioButton();
    noTreatmentRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuotePersonalInfoPage.class);
  }

  /**
   * Complete the DIQuoteEligibility details for alternative path one.
   *
   * @return the DIQuoteEligibility page object
   */

  @Step("Complete TLI Eligibility Page Questions for AltPath2")
  public DiQuotePersonalInfoPage completeDIQuoteEligibilityQuestionsAltPathOne() {
    noChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noMilitaryRdo.setRadioButton();
    noDisabilityRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noTestingRdo.setRadioButton();
    noScheduledRdo.setRadioButton();
    noDrugRdo.setRadioButton();
    noTreatmentRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuotePersonalInfoPage.class);
  }

  /**
   * Complete the DIQuoteEligibility details for alternative path two.
   *
   * @return the DIQuoteEligibility page object
   */

  @Step("Complete TLI Eligibility Page Questions for AltPath2")
  public DiQuotePersonalInfoPage completeDIQuoteEligibilityQuestionsAltPathTwo() {
    yesChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    noMilitaryRdo.setRadioButton();
    noDisabilityRdo.setRadioButton();
    noAIDSRdo.setRadioButton();
    noTestingRdo.setRadioButton();
    noScheduledRdo.setRadioButton();
    noDrugRdo.setRadioButton();
    noTreatmentRdo.setRadioButton();
    nextBtn.click();
    return navigatingTo(DiQuotePersonalInfoPage.class);
  }

  /**
   * Enter the negative user details for the TLI Eligibility page Error Messages
   */

  @Step("Fill in the DI Eligibility Page No Christian No Spouse")
  public DiQuoteEligibilityPage diNoNoChristianSpouseFill() {
    noChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Christian No Spouse")
  public DiQuoteEligibilityPage diYesNoChristianSpouseFill() {
    yesChristianRdo.setRadioButton();
    noChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page No Christian No Spouse")
  public DiQuoteEligibilityPage diNoYesChristianSpouseFill() {
    noChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Christian Yes Spouse")
  public DiQuoteEligibilityPage diYesYesChristianSpouseFill() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    return this;
  }
  @Step("Fill in the DI Eligibility Page No US Citizen")
  public DiQuoteEligibilityPage diNoUSCitizenFill() {
    noUSCitizenRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Military")
  public DiQuoteEligibilityPage diYesMilitaryFill() {
    yesMilitaryRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Disability")
  public DiQuoteEligibilityPage diYesDisabilityFill() {
    yesDisabilityRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes AIDS")
  public DiQuoteEligibilityPage diYesAidsFill() {
    yesAIDSRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Testing")
  public DiQuoteEligibilityPage diYesTestingFill() {
    yesTestingRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Scheduled")
  public DiQuoteEligibilityPage diYesScheduledFill() {
    yesScheduledRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Drug")
  public DiQuoteEligibilityPage diYesDrugFill() {
    yesDrugRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Yes Treatment")
  public DiQuoteEligibilityPage diYestreatmentFill() {
    yesTreatmentRdo.setRadioButton();
    return this;
  }

  @Step("Fill in the DI Eligibility Page Eligibility")
  public DiQuoteEligibilityPage diEligibleFill() {
    yesChristianRdo.setRadioButton();
    yesChristianSpouseRdo.setRadioButton();
    yesUSCitizenRdo.setRadioButton();
    yesMilitaryRdo.setRadioButton();
    yesDisabilityRdo.setRadioButton();
    yesAIDSRdo.setRadioButton();
    yesTestingRdo.setRadioButton();
    yesScheduledRdo.setRadioButton();
    yesDrugRdo.setRadioButton();
    yesTreatmentRdo.setRadioButton();
    return this;
  }

}