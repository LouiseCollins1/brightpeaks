package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.Select;
import com.sqs.web.elements.TextInput;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 04/04/2017.
 */
public class EssaQuoteFundAccountPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));

  //TextInput
  private final TextInput accountNoInput = new TextInput(By.id("accountnumber"));
  private final TextInput routingInput = new TextInput(By.id("routingnumber"));
  private final TextInput depositInput = new TextInput(By.id("initialdepositamount"));

  //Select
  private final Select accountType = new Select(By.id("banking"));

  //Button
  private final Button continueBtn = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaQuoteFundAccountPage.
   */
  public EssaQuoteFundAccountPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Fund Account Information Page has successfully loaded.");
  }

  /**
   * Enter the account details
   *
   * @return the ESSA Quote Fund Account Info page object
   */

  @Step("Complete ESSA Fund Account Page Questions")
  public EssaQuoteRecurringDepositsInfoPage completeESSAFundAccountInfo() {
    String accountNo = "145465465";
    String routingNo = "091000019";
    String deposit = "100";

    accountNoInput.setText(accountNo);
    accountType.sendKeys("Checking");
    routingInput.setText(routingNo);
    depositInput.setText(deposit);
    continueBtn.click();
    return navigatingTo(EssaQuoteRecurringDepositsInfoPage.class);
  }

}
