package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 03/04/2017.
 */

public class EmergencySavingsPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  private final Hyperlink openAccountLink = new Hyperlink(By.id("btn-hero-emergency-savings"));

  /**
   * Instantiates a new Brightpeak ESSAPage.
   */
  public EmergencySavingsPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("Emergency Savings Page has successfully loaded.");
  }

  /**
   * click on the Get Me Free Quote link
   *
   * @return to questionnaire for ESSA
   */

  @Step("Navigate from Emergency Savings Account page to free quote")
  public EssaQuoteGetStartedPage clickOpenLink() {
    openAccountLink.click();
    return navigatingTo(EssaQuoteGetStartedPage.class);
  }

}