package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.Select;
import com.sqs.web.elements.TextInput;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class EssaQuotePersonalInfoPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));
  //TextInput
  private final TextInput firstNameInput = new TextInput(By.id("firstname"));
  private final TextInput middleNameInput = new TextInput(By.id("middleinitial"));
  private final TextInput lastNameInput = new TextInput(By.id("lastname"));
  private final TextInput dobInput = new TextInput(By.id("dateofbirth"));
  private final TextInput phoneInput = new TextInput(By.id("phone"));
  private final TextInput ssnInput = new TextInput(By.id("ssn"));
  private final TextInput driversLicenseNo = new TextInput(By.id("driverslicense"));
  private final TextInput issueDateInput = new TextInput(By.id("dlissuedate"));
  private final TextInput expirationDateInput = new TextInput(By.id("dlexpiredate"));
  private final TextInput address1Input = new TextInput(By.id("address1"));
  private final TextInput address2Input = new TextInput(By.id("address2"));
  private final TextInput cityInput = new TextInput(By.id("city"));
  private final TextInput zipInput = new TextInput(By.id("zipcode"));
  //Select
  private final Select gender = new Select(By.id("gender"));
  private final Select licenseState = new Select(By.id("dlstate"));
  private final Select state = new Select(By.id("state"));
  //Button
  private final Button continueButton = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaQuotePersonalInfoPage.
   */
  public EssaQuotePersonalInfoPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Personal Information Page has successfully loaded.");
  }

  /**
   * Enter the user details
   *
   * @return the ESSA Personal Information Page object
   */

  @Step("Complete ESSA Personal Information Page Questions")
  public EssaQuoteFundAccountPage completeESSPersonalInfoPage() {
    String firstName = "Testname-sqs-" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
    String middleName = "A";
    String lastName = "Lastname" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
    String dateOfBirth = "01011970";
    String phoneNumber = "612555" + RandomStringUtils.randomNumeric(4);
    String socialSecurity = "476" + RandomStringUtils.randomNumeric(6);
    String licenseNo = "C45456464564";
    String issued = "012015";
    String expiration = "01012020";
    String address1 = "123 Main St";
    String address2 = "#100";
    String city = "Minneapolis";
    String zip = "55413";

    firstNameInput.setText(firstName);
    middleNameInput.setText(middleName);
    lastNameInput.setText(lastName);
    dobInput.setText(dateOfBirth);
    gender.sendKeys("Female");
    phoneInput.setText(phoneNumber);
    ssnInput.setText(socialSecurity);
    driversLicenseNo.setText(licenseNo);
    licenseState.sendKeys("Minnesota");
    issueDateInput.setText(issued);
    expirationDateInput.setText(expiration);
    address1Input.setText(address1);
    address2Input.setText(address2);
    cityInput.setText(city);
    state.sendKeys("Minnesota");
    zipInput.setText(zip);
    continueButton.click();
    return navigatingTo(EssaQuoteFundAccountPage.class);
  }

}
