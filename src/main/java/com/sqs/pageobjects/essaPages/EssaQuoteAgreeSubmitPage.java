package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.CheckBox;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 04/04/2017.
 */
public class EssaQuoteAgreeSubmitPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));

  //Checkbox
  private final CheckBox agreementBox = new CheckBox(By.id("agreementFinal"));

  //Button
  private final Button submitBtn = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaQuoteAgreeSubmitPage.
   */
  public EssaQuoteAgreeSubmitPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Agree and Submit Page has successfully loaded.");
  }

  /**
   * Enter the account details
   *
   * @return the ESSA Quote Agreement page object
   */
  @Step("Agree with the T&Cs and Submit application")
  public EssaQuoteAgreeSubmitPage completeESSAAgreeAndSubmit() {
    agreementBox.click();
    submitBtn.click();
    return this;
  }
}
