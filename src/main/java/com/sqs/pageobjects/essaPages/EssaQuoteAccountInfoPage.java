package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.Select;
import com.sqs.web.elements.TextInput;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 03/04/2017.
 */

public class EssaQuoteAccountInfoPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));

  //TextInput
  private final TextInput emailInput = new TextInput(By.id("emailaddress"));

  //Select
  private final Select accountType = new Select(By.id("accounttype"));
  private final Select membershipBasis = new Select(By.id("primchristian"));


  //Button
  private final Button continueBtn = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaQuoteGetStartedPage.
   */
  public EssaQuoteAccountInfoPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Account Page has successfully loaded.");
  }

  /**
   * Enter the account details
   *
   * @return the ESSA Quote Account Info page object
   */

  @Step("Complete ESSA Account Info Page Questions")
  public EssaQuotePersonalInfoPage completeESSAAccountInfo() {
    String email = "faith.chapman@test.brightpeakfinancial.com";

    emailInput.click();
    emailInput.setText(email);
    accountType.sendKeys("Individual Account ");
    membershipBasis.sendKeys("A Christian");
    continueBtn.click();
    return navigatingTo(EssaQuotePersonalInfoPage.class);
  }

}