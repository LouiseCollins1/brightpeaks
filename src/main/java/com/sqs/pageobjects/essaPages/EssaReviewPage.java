package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class EssaReviewPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));
  //Button
  private final Button continueBtn = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaReviewPage.
   */
  public EssaReviewPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Review Page has successfully loaded.");
  }

  /**
   * Enter the account details
   *
   * @return the ESSA Quote Review page object
   */
  @Step("Complete ESSA Review Page Questions")
  public EssaQuoteAgreeSubmitPage completeESSAReviewPage() {
    continueBtn.click();
    return navigatingTo(EssaQuoteAgreeSubmitPage.class);
  }
}
