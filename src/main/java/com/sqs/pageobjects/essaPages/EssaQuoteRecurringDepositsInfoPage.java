package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import com.sqs.web.elements.Select;
import com.sqs.web.elements.TextInput;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by DillonG on 04/04/2017.
 */
public class EssaQuoteRecurringDepositsInfoPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));
  //Select
  private final Select depositFrequency = new Select(By.id("ongoingdeposits"));
  private final Select depositDayInput = new Select(By.id("depositdate"));
  //TextInput
  private final TextInput depositAmountInput = new TextInput(By.id("depositamount"));
  //Button
  private final Button continueBtn = new Button(By.id("continue"));

  /**
   * Instantiates a new Brightpeak EssaQuoteRecurringDepositsInfoPage.
   */
  public EssaQuoteRecurringDepositsInfoPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Recurring Deposit Information Page has successfully loaded.");
  }

  /**
   * Enter the account details
   *
   * @return the ESSA Quote Recurring Deposits Info page object
   */
  @Step("Complete ESSA Recurring Deposits Page Questions")
  public EssaReviewPage completeESSARecurringDepositsPage() {
    String amount = "100";

    depositFrequency.sendKeys("Monthly deposits");
    depositAmountInput.setText(amount);
    depositDayInput.sendKeys("15th");
    continueBtn.click();
    return navigatingTo(EssaReviewPage.class);
  }
}