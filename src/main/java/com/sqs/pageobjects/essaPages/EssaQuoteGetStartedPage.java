package com.sqs.pageobjects.essaPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Label;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class EssaQuoteGetStartedPage extends PageObject {

  //Label
  private final Label brightpeakFinancialLogo = new Label(By.xpath("//img[@alt='brightpeak financial']"));

  //Button
  private final Button begin = new Button(By.xpath("//a[contains(text(), 'BEGIN')]"));

  /**
   * Instantiates a new Brightpeak EssaQuoteGetStartedPage.
   */
  public EssaQuoteGetStartedPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("ESSA Quote Get Started Page has successfully loaded.");
  }

  /**
   * Enter the user details
   *
   * @return the DI Quote About page object
   */

  @Step("Click 'Begin' to navigate to questions")
  public EssaQuoteAccountInfoPage clickBeginToGetStarted() {
    begin.click();
    return navigatingTo(EssaQuoteAccountInfoPage.class);
  }

}
