package com.sqs.pageobjects.gripPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.web.elements.Hyperlink;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * The Brightpeak grip page.
 */
public class GuaranteedRetirementPage extends PageObject {

  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  private final Hyperlink getMyFreeDIQuoteLink = new Hyperlink(By.id("btn-guaranteed-retirement-plan"));

  /**
   * Instantiates a new Brightpeak gripPage.
   */
  public GuaranteedRetirementPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("Guaranteed Retirement Page has successfully loaded.");
  }

  /**
   * click on the Get Me Free Quote link
   *
   * @return to questionnaire for GRIP
   */

  @Step("Navigate from Guaranteed Retirement Insurance page to free quote")
  public GripQuoteAboutPage clickGetMyFreeQuoteLink() {
    getMyFreeDIQuoteLink.click();
    return navigatingTo(GripQuoteAboutPage.class);
  }

}