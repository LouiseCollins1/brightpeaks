package com.sqs.pageobjects.gripPages;

import com.sqs.common.PageObject;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.elements.Label;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class GripNextStepsPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //Label
  private final Label nextStepsLabel = new Label(By.xpath("//label[contains(text(),'Thank you very much for your interest')]"));

  GripNextStepsPage gripNextStepsPage;

  /**
   * Instantiates a new Brightpeak GripQuoteAboutPage.
   */
  public GripNextStepsPage() {
  }

  @Override
  public void onPage() {
    brightpeakFinancialLogo.isDisplayed();
  }

  /**
   * Verify the information message is displayed on the page
   *
   * @return the GripNextStepsPage page object
   */
  @Step("Next Steps information message is displayed")
  public GripNextStepsPage verifyInformationMessageDisplayed() {
    nextStepsLabel.isDisplayed();
    return this;
  }

}
