package com.sqs.pageobjects.gripPages;

import com.sqs.common.PageObject;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.core.common.CommonActions;
import com.sqs.web.elements.*;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

public class GripQuoteAboutPage extends PageObject {

  //Links
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));

  //TextInput
  private final TextInput firstNameInput = new TextInput(By.id("firstname"));
  private final TextInput lastNameInput = new TextInput(By.id("lastname"));
  private final TextInput emailInput = new TextInput(By.id("email"));
  private final TextInput phoneNumberInput = new TextInput(By.xpath("//input[@name='phone']"));
  private final TextInput zipCodeInput = new TextInput(By.xpath("//input[@name='zip']"));
  private final TextInput dobInput = new TextInput(By.xpath("//input[@name='dob']"));
  private final TextInput retirementAgeInput = new TextInput(By.xpath("//input[@name='retirementage']"));
  private final TextInput monthlyContributionInput = new TextInput(By.xpath("//input[@name='monthlydeposit']"));

  //RadioButtons
  private final RadioButton femaleRadio = new RadioButton(By.xpath("//*[@name='gender']/*[@value='Female']"));

  //Button
  private final Button submitButton = new Button(By.xpath("//span[contains(text(), 'Submit')]"));


  /**
   * Instantiates a new Brightpeak GripQuoteAboutPage.
   */
  public GripQuoteAboutPage() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("GRIP Quote About Page has successfully loaded.");
  }

  /**
   * Enter the user details
   *
   * @return the GRIP Quote About page object
   */
  @Step("Complete GRIP About Page Questions and submit application")
  public GripNextStepsPage completeGRIPQuestionsAndSubmit() {
    String firstName = "Testname-sqs-" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
    String lastName = "Lastname" + RandomStringUtils.randomAlphabetic(5).toLowerCase();
    String email = "faith.chapman@test.brightpeakfinancial.com";
    String phoneNumber = "612555" + RandomStringUtils.randomNumeric(4);
    String zipCode = "55401";
    String dateOfBirth = "01011970";
    String retirementAge = "65";
    String mContribution = "100";
    firstNameInput.clear();

    firstNameInput.setText(firstName);
    lastNameInput.setText(lastName);
    emailInput.setText(email);
    phoneNumberInput.setText(phoneNumber);
    zipCodeInput.setText(zipCode);
    dobInput.setText(dateOfBirth);
    femaleRadio.setRadioButton();
    retirementAgeInput.setText(retirementAge);
    monthlyContributionInput.setText(mContribution);
    CommonActions.pauseTest(2);
    submitButton.click();
    return navigatingTo(GripNextStepsPage.class);
  }

}