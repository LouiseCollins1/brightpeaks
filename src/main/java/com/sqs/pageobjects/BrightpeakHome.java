package com.sqs.pageobjects;

import com.sqs.common.PageObject;
import com.sqs.core.common.CommonActions;
import com.sqs.core.sqslibs.WebLog;
import com.sqs.pageobjects.diPages.DisabilityInsurancePage;
import com.sqs.pageobjects.essaPages.EmergencySavingsPage;
import com.sqs.pageobjects.gripPages.GuaranteedRetirementPage;
import com.sqs.pageobjects.tliPages.TermLifeInsurancePage;
import com.sqs.web.elements.Button;
import com.sqs.web.elements.Hyperlink;
import com.sqs.web.webdriver.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The Brightpeak home page.
 */
public class BrightpeakHome extends PageObject {

  final ArrayList<String> list = new ArrayList<String>();
  private final Hyperlink brightpeakFinancialLogo = new Hyperlink(By.xpath("//div[@class=' header-logo fit-logo-img add-header-height logo-is-responsive logo-has-sticky']/a/img[1]"));
  //Products Nav items
  private final Hyperlink productsLink = new Hyperlink(By.id("menu-item-link-products"));
  private final Hyperlink emergencySavingsInsurance = new Hyperlink(By.id("menu-item-link-emergency-savings-account"));
  private final Hyperlink termLifeInsurance = new Hyperlink(By.id("menu-item-term-life-insurance"));
  private final Hyperlink disabilityInsurance = new Hyperlink(By.id("menu-item-link-disability-insurance"));
  private final Hyperlink guaranteedRetirementInsurance = new Hyperlink(By.id("menu-item-link-guaranteed-retirement-plan"));
  private final Hyperlink studentLoanInsurance = new Hyperlink(By.id("menu-item-link-student-loan-refinancing"));
  //Resources Nav items
  private final Hyperlink resourcesLink = new Hyperlink(By.id("menu-item-link-resources"));
  private final Hyperlink savingsChallenge = new Hyperlink(By.id("menu-item-link-savings-challenge"));
  //About Nav items
  private final Hyperlink aboutLink = new Hyperlink(By.id("menu-item-link-about"));
  private final Hyperlink companyLink = new Hyperlink(By.id("menu-item-link-company"));
  private final Hyperlink membershipLink = new Hyperlink(By.id("menu-item-link-membershipmenu-item-link-membership"));
  private final Hyperlink contactUsLink = new Hyperlink(By.id("menu-item-link-contact-us"));
  //Blog Nav items
  private final Hyperlink blogLink = new Hyperlink(By.id("menu-item-link-blog"));
  private final Button signMeUpButton = new Button(By.xpath(".//div[@class='actions']//input"));

  /**
   * Instantiates a new Brightpeak home.
   */
  public BrightpeakHome() {
  }

  @Override
  public void onPage() {
    OnPageElementMethod(brightpeakFinancialLogo);
    WebLog.info("Brightpeak home has successfully loaded.");
  }

  /**
   * Launches the web browser and Brightpeak homepage.
   */
  public void init() {
    String URL = "https://test.brightpeakfinancial.com";
    DriverProvider.getDriver().get(URL);
    //Ensure we are on the correct page
    onPage();
  }

  /**
   * click on the Products link.
   *
   * @return the list of product navigation options on BrightpeakHome page
   */
  public BrightpeakHome clickProductsLink() {
    CommonActions.pauseTest(2);
    productsLink.click();
    return this;
  }

  /**
   * Click the TLI page link.
   *
   * @return navigating to the TLI page object
   */
  public TermLifeInsurancePage clickTermLifeInsuranceLink() {
    termLifeInsurance.click();
    return navigatingTo(TermLifeInsurancePage.class);
  }

  /**
   * Click the DI page link.
   *
   * @return navigating to the DI page object
   */
  public DisabilityInsurancePage clickDisabilityInsuranceLink() {
    disabilityInsurance.click();
    return navigatingTo(DisabilityInsurancePage.class);
  }

  /**
   * Click the GRIP page link.
   *
   * @return navigating to the GRIP page object
   */
  public GuaranteedRetirementPage clickGuaranteedRetirementLink() {
    guaranteedRetirementInsurance.click();
    return navigatingTo(GuaranteedRetirementPage.class);
  }

  /**
   * Click the ESSA page link.
   *
   * @return navigating to the ESSA page object
   */
  public EmergencySavingsPage clickEmergencySavingsLink() {
    emergencySavingsInsurance.click();
    return navigatingTo(EmergencySavingsPage.class);
  }

  /**
   * Launch the TLI page.
   *
   * @return the TLI page object
   */
  public TermLifeInsurancePage launchTLIPage() {
    init();
    clickProductsLink();
    clickTermLifeInsuranceLink();
    return navigatingTo(TermLifeInsurancePage.class);
  }

  /**
   * Launch the DI page.
   *
   * @return the DI page object
   */
  public DisabilityInsurancePage launchDIPage() {
    init();
    clickProductsLink();
    clickDisabilityInsuranceLink();
    return navigatingTo(DisabilityInsurancePage.class);
  }

  /**
   * Launch the GRIP page.
   *
   * @return the GRIP page object
   */
  public GuaranteedRetirementPage launchGRIPPage() {
    init();
    clickProductsLink();
    clickGuaranteedRetirementLink();
    return navigatingTo(GuaranteedRetirementPage.class);
  }

  /**
   * Launch the ESSA page.
   *
   * @return the ESSA page object
   */
  public EmergencySavingsPage launchESSAPage() {
    init();
    clickProductsLink();
    clickEmergencySavingsLink();
    return navigatingTo(EmergencySavingsPage.class);
  }

  /**
    Method which invokes a response from the links server to check they are OK.
   *
   * @return the bad links
   */
  public List<String> linkCheck(WebElement element) throws IOException {
    init();
    List<WebElement> links = element.findElements(By.tagName("a"));
    List<String> badLinks = new ArrayList<>();
    HttpURLConnection client = null;
    for (WebElement link : links) {
      // set up httpclient
      // ping link
      String href = link.getAttribute("href");
      URL url = new URL(href);
      client = (HttpURLConnection) url.openConnection();
      client.setRequestMethod("GET");
      client.connect();
      if (client.getResponseCode() != HttpURLConnection.HTTP_OK) {
        badLinks.add(href);
      }
    }
    return badLinks;
  }

}
